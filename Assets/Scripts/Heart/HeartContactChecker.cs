using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartContactChecker : BaseContactChecker
{
    protected override void OnCollisionContactEnter(Collision2D otherCollision)
    {

    }

    protected override void OnTriggerContactEnter(Collider2D otherCollision)
    {
        if (otherCollision.CompareTag("Player"))
        {
            GameManager.Instance.damageLogic.HealPlayer(1);
            Destroy(gameObject);
        }
    }
}