using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    PlayerHealth playerHealth;
    Collider2D heartCollider;
    MagneticProperty magneticProperty;

    private void Start()
    {
        playerHealth = GameManager.Instance.playerHealth;
        TryGetComponent(out heartCollider);
        TryGetComponent(out magneticProperty);
    }

    private void Update()
    {
        if((playerHealth.currentHealth > 0) && (playerHealth.currentHealth < playerHealth.maxHealth))
        {
            heartCollider.enabled = true;
            magneticProperty.enabled = true;
        }
        else
        {
            heartCollider.enabled = false;
            magneticProperty.enabled = false;
        }
    }
}