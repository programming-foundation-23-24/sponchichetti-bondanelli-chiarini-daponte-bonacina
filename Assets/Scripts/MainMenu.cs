using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(StartSoundtrackAfterDelay(1f));
    }

    IEnumerator StartSoundtrackAfterDelay(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        SoundManager.Instance.PlaySoundtrack(SoundType.ST_titleScreen, .3f);
    }

    public void ExitMainMenu()
    {
        StopAllCoroutines();
        SoundManager.Instance.StopSoundtrack();
    }
}