using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimPointerLine : MonoBehaviour
{
    LineRenderer lineRenderer;
    Vector3[] vector3s = new Vector3[2];

    private void Awake()
    {
        if (!TryGetComponent(out lineRenderer))
        {
            Debug.LogError("Line renderer missing component");
        }
    }

    private void Update()
    {
        vector3s[0] = GameManager.Instance.GetPlayerSpellShooterPosition();
        vector3s[1] = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        lineRenderer.SetPositions(vector3s);
    }
}
