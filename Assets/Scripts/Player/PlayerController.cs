using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform bodyTransform;
    [SerializeField] DanceFloorController danceFloorController;
    [SerializeField] ParticleSystem channelingParticles;
    [SerializeField] PlayerSpellShapesDatabase playerSpellInventory;
    [SerializeField] SpellShooter spellShooter;
    [SerializeField] Animator animator;

    public GameObject dancefloor;

    [Range(1.0f, 7f)] public float movementSpeed = 1;
    public string buttonMagicPlane;
    public float danceOffset = 1;
    public float danceStepDuration = 0.5f;
    public float danceStepCooldown = 0.5f;
    public float waitTimeBeforeNextDanceInput = .05f;

    public Element activeElement;

    public float knockbackDistance;
    public float knockbackDuration; //deve essere minore della durata del damage buffer (PlayerHealth)
    public AnimationCurve knockbackCurve = AnimationCurve.Linear(0, 0, 1, 1);
    bool immobilized;
    //bool waitingAfterShooting;

    public bool isMagicActive = false; // BOOL temporaneo, sar� gestito dal GM
    public float verticalMovement;
    public float horizontalMovement;

    Vector3 bodyDefaultOffset;
    bool canStep = true;
    public DanceInput nextKeyCombo;
    public int comboSize;
    List<DanceInput> spellKeyComboList = new List<DanceInput>();
    DanceInput selectedShapeSlot;
    public DanceInput firstInputInCoreomancy;
    public DanceInput lastInputInCoreomancy;
    DanceInput lastPlayerDanceInput;

    int numberOfStepsDoneInThisDanceSession;

    void Awake()
    {
        bodyDefaultOffset = bodyTransform.position;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void Update()
    {
        if (GameManager.Instance.gameFSM.currentState != State.GamePlaying)
        {
            return;
        }

        if (immobilized/* || waitingAfterShooting*/)
        {
            return;
        }

        verticalMovement = Input.GetAxisRaw("Vertical");
        horizontalMovement = Input.GetAxisRaw("Horizontal");

        if (isMagicActive)
        {
            if (!Input.GetButton(buttonMagicPlane))
            {
                ToggleMagicPlane(false);
            }

            PlayerDanceInput();     //qui dove controlla gli input e si salva solo l'ultimo eseguito

            PlayerDance();          //qui dove esegue il passo in base all'ultimo input    
        }
        else
        {
            PlayerMovement();
            
            if (Input.GetButtonDown(buttonMagicPlane))
            {
                ToggleMagicPlane(true);
            }
        }       
    }

    void PlayerMovement()
    {
        float speedBoost = GameManager.Instance.damageLogic.actualSpeedBoost;
        Vector3 movement = new Vector3(horizontalMovement, verticalMovement).normalized * (movementSpeed + speedBoost) * Time.deltaTime;
        if (!GameManager.Instance.CheckIfOutOfBounds(transform.position + movement))
        {
            transform.position += movement;
        }
        if (Mathf.Abs(movement.magnitude) > 0)
        {
            if (GameManager.Instance.damageLogic.actualSpeedBoost <= 0f)
            {
                animator.SetTrigger("Run");
            }
            else
            {
                animator.SetTrigger("Moonwalk");
            }
        }
        else
        {
            animator.SetTrigger("Idle");
        }
    }

    void PlayerDanceInput()
    {
        if (DanceInputAlreadyTaken())
        {
            return;
        }

        if (Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
        {
            if (horizontalMovement > 0)         //right
            {
                lastPlayerDanceInput = DanceInput.Right;
            }
            else if (horizontalMovement < 0)    //left
            {
                lastPlayerDanceInput = DanceInput.Left;
            }
            else if (verticalMovement > 0)      //up
            {
                lastPlayerDanceInput = DanceInput.Up;
            }
            else if (verticalMovement < 0)      //down
            {
                lastPlayerDanceInput = DanceInput.Down;
            }
        }
    }

    void PlayerDance()
    {
        animator.SetTrigger("Idle");

        if (canStep && DanceInputAlreadyTaken())
        {
            switch (lastPlayerDanceInput)
            {
                case DanceInput.Up:
                    CheckComboStep(DanceInput.Up);
                    animator.SetTrigger("PoseUp");
                    bodyTransform.position += new Vector3(0, danceOffset);
                    break;
                case DanceInput.Left:
                    CheckComboStep(DanceInput.Left);
                    animator.SetTrigger("PoseLeft");
                    bodyTransform.position += new Vector3(-danceOffset, 0);
                    break;
                case DanceInput.Down:
                    CheckComboStep(DanceInput.Down);
                    animator.SetTrigger("PoseDown");
                    bodyTransform.position += new Vector3(0, -danceOffset);
                    break;
                case DanceInput.Right:
                    CheckComboStep(DanceInput.Right);
                    animator.SetTrigger("PoseRight");
                    bodyTransform.position += new Vector3(danceOffset, 0);
                    break;
                default:
                    return;
            }
            
            StartCoroutine(DanceStep());
            StartCoroutine(DancePlayerInputBufferReset());
        }
    }

    bool DanceInputAlreadyTaken()
    {
        if (lastPlayerDanceInput != DanceInput.ShapeInput && lastPlayerDanceInput != DanceInput.ElementInput)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator DanceStep()
    {
        CameraShake.Instance.CameraShakeOnPlayerDancing();

        canStep = false;
        yield return new WaitForSeconds(danceStepDuration);

        bodyTransform.position = transform.position + bodyDefaultOffset;
        yield return new WaitForSeconds(danceStepCooldown);
        canStep = true;
    }

    IEnumerator DancePlayerInputBufferReset()
    {
        yield return new WaitForSeconds(waitTimeBeforeNextDanceInput);
        lastPlayerDanceInput = DanceInput.ShapeInput;
    }

    private void ToggleMagicPlane(bool isActive)
    {
        dancefloor.SetActive(isActive);
        isMagicActive = isActive;
        if (isActive)
        {
            GenerateSpellCombo();
            ToggleChannelingParticles(true);
            numberOfStepsDoneInThisDanceSession = 0;
            lastPlayerDanceInput = DanceInput.ShapeInput;
            GameManager.Instance.ActivateCoreomancy();
        }
        else
        {
            spellKeyComboList.Clear();
            ToggleChannelingParticles(false);
            GameManager.Instance.DeactivateCoreomancy();
        }
    }

    void ToggleChannelingParticles(bool isActive) 
    {
        if (isActive == true)
        {
            /*Color elementColor = GameManager.Instance.elementsColors[(int)activeElement];

            var main = channelingParticles.main;                                                        //x particelle colorate con elemento
            main.startColor = elementColor;*/               

            channelingParticles.Play();
        }
        else
        {
            channelingParticles.Stop();
        }
    }

    private void GenerateSpellCombo()
    {
        spellKeyComboList.Clear();
        selectedShapeSlot = DanceInput.ShapeInput;

        for (int i = 0; i < comboSize; i++)
        {
            spellKeyComboList.Add(DanceInput.Up);

            if (i == 0)
            {
                spellKeyComboList[i] = firstInputInCoreomancy;
            }
            else if (i == comboSize - 1)
            {
                spellKeyComboList[i] = lastInputInCoreomancy;
            }
            else
            {
                spellKeyComboList[i] = GenerateRandomInput();
            }
        }

        nextKeyCombo = spellKeyComboList[0];
        danceFloorController.SetPlatforms(nextKeyCombo);
    }

    DanceInput GenerateRandomInput()
    {
        switch(Random.Range(0, 4))
        {
            case 0:
                return DanceInput.Up;
            case 1:
                return DanceInput.Left;
            case 2:
                return DanceInput.Down;
            case 3:
                return DanceInput.Right;
            default: 
                return DanceInput.Up;
        }
    }


    private void CheckComboStep(DanceInput input)
    {

        if (nextKeyCombo == DanceInput.ElementInput)
        {
            switch (input)
            {
                case DanceInput.Up:
                    activeElement = Element.Fire;
                    break;
                case DanceInput.Left:
                    activeElement = Element.Thunder;
                    break;
                case DanceInput.Down:
                    activeElement = Element.Ice;
                    break;
                case DanceInput.Right:
                    activeElement = Element.Earth;
                    break;
            }
            ProceedCombo();
            return;
        }
        else if (nextKeyCombo == DanceInput.ShapeInput)
        {
            selectedShapeSlot = input;
            ProceedCombo();
            return;
        }
        else if (input == nextKeyCombo) //se combo size > 2
        {
            ProceedCombo();
        }
        else
        {
            CoreomancyFailed();
        }
    }

    void ProceedCombo()
    {
        //SoundManager.Instance.PlaySoundFromIndex(SoundType.NormalCoreomancyStep, comboSize - spellKeyComboList.Count, .35f);

        SoundManager.Instance.PlaySoundFromIndex(SoundType.NormalCoreomancyStep, numberOfStepsDoneInThisDanceSession, false, .35f);
        if (numberOfStepsDoneInThisDanceSession < 7)
        {
            numberOfStepsDoneInThisDanceSession++;
        }

        spellKeyComboList.RemoveAt(0);
        if (spellKeyComboList.Count > 0)
        {
            nextKeyCombo = spellKeyComboList[0];
            danceFloorController.SetPlatforms(nextKeyCombo);
        }
        else
        {
            CoreomancyCompleted();
        }
    }

    void CoreomancyCompleted()
    {
        spellShooter.ShootSpell(selectedShapeSlot);

        GenerateSpellCombo();

        //CODICE PER FAR SI CHE LA COREOMANZIA FINISCA DOPO IL PRIMO COLPO
        //StartCoroutine(WaitAfterShooting());
        //ToggleMagicPlane(false);
    }

    public void CoreomancyFailed()
    {
        ToggleMagicPlane(false);
    }

    /*
    IEnumerator WaitAfterShooting()
    {
        waitingAfterShooting = true;
        yield return new WaitForSeconds(.1f);
        waitingAfterShooting = false;
    }
    */

    public IEnumerator Knockback(Vector3 pusherPosition)
    {
        //ignora eventuali ostacoli interni all'aerea di gioco
        immobilized = true;

        Vector3 startingPosition = transform.position;
        Vector3 destination = transform.position + (startingPosition - pusherPosition).normalized * knockbackDistance;

        float elapsedTime = 0;

        while (elapsedTime < knockbackDuration)
        {
            float normalizedElapsedTime = Mathf.InverseLerp(0.0f, knockbackDuration, elapsedTime);
            float evaluatedTimeByCurve = knockbackCurve.Evaluate(normalizedElapsedTime);

            Vector2 nextPosition = Vector2.Lerp(startingPosition, destination, evaluatedTimeByCurve);
            if (GameManager.Instance.CheckIfOutOfBounds(nextPosition))
            {
                break;
            }
            else
            {
                transform.position = nextPosition;
            }

            elapsedTime += Time.deltaTime;

            yield return null;
        }

        if (!GameManager.Instance.CheckIfOutOfBounds(destination))
        {
            transform.position = destination;
        }

        immobilized = false;
        yield return null;
    }
}