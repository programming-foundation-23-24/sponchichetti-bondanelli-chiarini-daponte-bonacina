using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimPointer : MonoBehaviour
{
    void Update()
    {
        if (GameManager.Instance.gameFSM.currentState != State.GamePaused)
        {
            transform.right = ((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - (Vector2)transform.position).normalized;
        }
    }
}