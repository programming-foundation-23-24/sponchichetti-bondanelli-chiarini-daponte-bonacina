using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

public class FlipPlayerSprite : MonoBehaviour
{
    public PlayerController player;
    public SpriteRenderer playerSpriteRenderer;

    public void Update()
    {
        if (!player.isMagicActive)
        {
            if (player.horizontalMovement < 0)
            {
                playerSpriteRenderer.flipX = true;
            }
            else if (player.horizontalMovement > 0)
            {
                playerSpriteRenderer.flipX = false;
            }
        }
        else
        {
            playerSpriteRenderer.flipX = false;
        }

    }
}
