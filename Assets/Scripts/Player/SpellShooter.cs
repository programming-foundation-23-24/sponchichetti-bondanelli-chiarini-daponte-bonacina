using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows;

public class SpellShooter : MonoBehaviour
{
    public void ShootSpell(DanceInput input)
    {
        PlayerSlot selectedShape = PlayerSpellShapesDatabase.Instance.GetPlayerAcquiredShapeFromInput(input);
        BaseProjectile projectile = Instantiate(selectedShape.projectile, transform.position, Quaternion.identity);
        projectile.projectileLevel = selectedShape.shapeLevel;
    }
}