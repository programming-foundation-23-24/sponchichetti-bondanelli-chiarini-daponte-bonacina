using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoostController
    : MonoBehaviour
{
    [SerializeField] ParticleSystem speedBoostParticles;
    [SerializeField] float particleMultiplierPerSpeedBoostValue = 20f;
    private void Start()
    {
        speedBoostParticles = GetComponent<ParticleSystem>();
        var main = speedBoostParticles.main;
        Color boostColor = GameManager.Instance.elementsColors[2];
        main.startColor = boostColor;
    }
    void Update()
    {
        var emissionPerBoost = speedBoostParticles.emission;
        emissionPerBoost.rateOverTime = GameManager.Instance.damageLogic.actualSpeedBoost * particleMultiplierPerSpeedBoostValue;
    }
}
