﻿using UnityEngine;

public class PlayerContactChecker : BaseContactChecker
{
    protected override void OnCollisionContactEnter(Collision2D otherCollision)
    {
        if (otherCollision.collider.GetComponent<BaseEnemy>() != null)
        {
            GameManager.Instance.PlayerGotHit(otherCollision.transform.position, true);
        }
    }

    protected override void OnTriggerContactEnter(Collider2D otherCollision)
    {
        if (otherCollision.GetComponent<EnemyProjectile>() != null)
        {
            GameManager.Instance.PlayerGotHit(otherCollision.transform.position, false);
        }

        
    }
}