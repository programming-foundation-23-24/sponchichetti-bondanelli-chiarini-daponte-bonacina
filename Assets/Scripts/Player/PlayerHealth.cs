using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 3;
    public int currentHealth;

    public bool tookDamageRecently;
    public float damageBufferDuration;

    public GameObject deathVFX;

    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Collider2D playerCollider;
    [SerializeField] PlayerController playerController;
    [SerializeField] GameObject bodyGO;


    private void Awake()
    {
        if (playerController == null)
        {
            TryGetComponent(out playerController);
        }

        currentHealth = maxHealth;
    }

    public IEnumerator PlayerGotHitVFX()
    {
        spriteRenderer.color = Color.gray;
        yield return new WaitForSeconds(.1f);
        spriteRenderer.color = Color.white;
        yield return new WaitForSeconds(.1f);
        spriteRenderer.color = Color.gray;
        yield return new WaitForSeconds(damageBufferDuration-.2f);
        spriteRenderer.color = Color.white;
    }
    
    public IEnumerator ResetDamageBuffer()
    {
        tookDamageRecently = true;
        yield return new WaitForSeconds(damageBufferDuration);
        StartCoroutine(OnOffCollider());
        tookDamageRecently = false;
    }

    IEnumerator OnOffCollider()
    {
        playerCollider.enabled = false;
        yield return new WaitForSeconds(.0001f);
        playerCollider.enabled = true;
    }

    public bool CheckIfDead()
    {
        if (currentHealth <= 0)
        {
            GameManager.Instance.GameOver(false);
            Death();
            return true;
        }

        return false;
    }

    void Death()
    {
        bodyGO.SetActive(false);
        Instantiate(deathVFX, transform.position, Quaternion.identity);
        playerController.enabled = false;
    }
}