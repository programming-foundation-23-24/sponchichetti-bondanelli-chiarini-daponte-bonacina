using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] DanceInput danceInput;
    [SerializeField] SpriteRenderer spriteRendererBordo;
    [SerializeField] Sprite baseSprite;
    Color elementColor;
    [SerializeField] Color baseOnColor;
    [SerializeField] Color baseOffColor;
    [SerializeField] Sprite elementIcon;
    [SerializeField] GameObject icon;
    SpriteRenderer SpriteRendererIcon;

    [SerializeField] SpriteRenderer spriteRendererSfondoElemento;

    private void Awake()
    {
        GameManager gm = GameManager.Instance;

        elementColor = gm.elementsColors[(int)danceInput + 1];
        spriteRendererSfondoElemento.color = new Color(elementColor.r, elementColor.g, elementColor.b, .4f);
        SpriteRendererIcon = icon.GetComponent<SpriteRenderer>();
    }

    public void TurnOnElementalPlatform()
    {
        SpriteRendererIcon.sprite = elementIcon;
        spriteRendererBordo.color = elementColor;
        spriteRendererBordo.sprite = baseSprite;
        spriteRendererSfondoElemento.enabled = true;
        icon.SetActive(true);
    }

    public void TurnOnShapePlatform()
    {
        spriteRendererBordo.color = baseOnColor;
        spriteRendererSfondoElemento.enabled = false;
        SpriteRendererIcon.sprite = PlayerSpellShapesDatabase.Instance.GetPlayerAcquiredShapeFromInput(danceInput).shapeIcon;
        icon.SetActive(true);
    }

    public void TurnOnPlatform()
    {
        spriteRendererBordo.color = baseOnColor;
        spriteRendererSfondoElemento.enabled = false;
        spriteRendererBordo.sprite = baseSprite;
        icon.SetActive(false);
    }

    public void TurnOffPlatform()
    {
        spriteRendererBordo.color = baseOffColor;
        spriteRendererBordo.sprite = baseSprite;
        icon.SetActive(false);
    }
}