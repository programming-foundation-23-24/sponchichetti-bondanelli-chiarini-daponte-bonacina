using System.Collections.Generic;
using UnityEngine;

public class DanceFloorController : MonoBehaviour
{
    public List<Platform> platforms = new List<Platform>();

    public void SetPlatforms(DanceInput comboKeyCode)
    {
        switch (comboKeyCode)
        {
            case DanceInput.ElementInput:
                foreach (Platform platform in platforms)
                {
                    platform.TurnOnElementalPlatform();
                }
                break;
            default:
            // >=DanceInput.Up and <= DanceInput.Right:
                foreach (Platform platform in platforms)
                {
                    platform.TurnOffPlatform();
                }
                platforms[(int)comboKeyCode].TurnOnPlatform();
                break;
            case DanceInput.ShapeInput:
                foreach (Platform platform in platforms)
                {
                    platform.TurnOnShapePlatform();
                }
                break;
        }
    }
}