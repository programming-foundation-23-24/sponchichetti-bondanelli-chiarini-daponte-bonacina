using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSpellShapesDatabase : Singleton<PlayerSpellShapesDatabase>
{
    public int shapesCountToShowOnLevelUp = 3;
    public int upgradesCountToShowOnLevelUp = 3;
    public int shapeMaxLevel = 3;

    [Header("All not acquired shapes database")]
    public List<SpellShapeData> notAlreadyAcquiredShapes = new List<SpellShapeData>();

    [Header("All acquired shapes aka inventory")]
    public List<PlayerSlot> playerSlots = new List<PlayerSlot>();

    public event Action<List<SpellShapeData>> OnShapesChosenForLvlUp;
    public event Action<List<PlayerSlot>> OnSlotsChosenForLvlUp;

    public List<T> GetRandomUniqueElements<T>(List<T> inputList, int amount)
    {
        if(inputList.Count < amount)
        {
            Debug.LogError($"{this}GetRandomUniqueElements Error: amount requested is greater than list's count");
            return null;
        }

        List<T> cloneList = new List<T>(inputList);

        List<T> randomElements = new List<T>();

        while (amount > 0)
        {
            T selectedElement = cloneList[UnityEngine.Random.Range(0, cloneList.Count)];

            randomElements.Add(selectedElement);
            cloneList.Remove(selectedElement);

            amount--;
        }

        return randomElements;
    }

    public PlayerSlot GetPlayerAcquiredShapeFromInput(DanceInput input)
    {
        foreach (PlayerSlot acquiredShape in playerSlots)
        {
            if (acquiredShape.danceInput == input)
            {
                return acquiredShape;
            }
        }

        Debug.LogError($".........");
        return null;
    }

    //2
    public void ShapeSelectionPhase()
    {
        if (notAlreadyAcquiredShapes.Count >= shapesCountToShowOnLevelUp)
        {
            List<SpellShapeData> chosenShapesForLvlUp = GetRandomUniqueElements(notAlreadyAcquiredShapes, shapesCountToShowOnLevelUp);
            OnShapesChosenForLvlUp?.Invoke(chosenShapesForLvlUp);
        }
        else
        {
            Debug.LogError("Not enough shapes in database for a level up");
            ExitLevelUpRequest();
        }

    }

    //or

    public void UpgradeSelectionPhase()
    {
        List<PlayerSlot> notMaxedSlots = NotAlreadyAtMaxLevelSlots();
        if (notMaxedSlots.Count > 1)
        {
            List<PlayerSlot> chosenSlotsForLvlUp = GetRandomUniqueElements(notMaxedSlots, upgradesCountToShowOnLevelUp);
            OnSlotsChosenForLvlUp?.Invoke(chosenSlotsForLvlUp);
        }
        else if (notMaxedSlots.Count == 1)
        {
            UpgradeSpecificSlot(notMaxedSlots[0].danceInput);
        }
        else if (notMaxedSlots.Count < 1)
        {
            Debug.Log("TUTTI GLI SLOT SONO GI� MAX LEVEL");
            ExitLevelUpRequest();
        }
    }


    //3 da UI o simili
    public void AcquireSpecificShape(DanceInput danceInput, SpellShapeData newShape)
    {
        foreach (PlayerSlot slot in playerSlots)
        {
            if (slot.danceInput == danceInput)
            {
                slot.projectile = newShape.projectile;
                slot.shapeIcon = newShape.icon;
                slot.shapeName = newShape.shapeName;
                slot.locked = true;
            }
        }

        notAlreadyAcquiredShapes.Remove(newShape);

        ExitLevelUpRequest();
    }

    //or

    public void UpgradeSpecificSlot(DanceInput danceInput)
    {
        foreach (PlayerSlot slot in playerSlots)
        {
            if (slot.danceInput == danceInput)
            {
                slot.shapeLevel++;
            }
        }

        ExitLevelUpRequest();
    }



    public bool AllShapesSlotsAreLockedIn()
    {
        int count = 0;

        foreach (PlayerSlot slot in playerSlots)
        {
            if (slot.locked)
            {
                count++;
            }
        }

        return count >= 4;
    }

    List<PlayerSlot> NotAlreadyAtMaxLevelSlots()
    {
        List<PlayerSlot> notMaxedSlots = new List<PlayerSlot>(playerSlots);

        foreach(PlayerSlot slot in playerSlots)
        {
            if(slot.shapeLevel >= shapeMaxLevel)
            {
                notMaxedSlots.Remove(slot);
            }
        }

        return notMaxedSlots;
    }

    void ExitLevelUpRequest()
    {
        GameManager.Instance.EndLevelUp();
    }
}