using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : Singleton<CameraShake>
{
    private CinemachineVirtualCamera cam;
    private float shakeTimer;

    [Header("Dance Step Camera Shake")]
    [SerializeField] float danceShakeIntensity = 2f;
    [SerializeField] float danceShakeTime = 0.3f;

    [Header("Player got hit Camera Shake")]
    [SerializeField] float playerHitShakeIntensity = 1f;
    [SerializeField] float playerHitShakeTime = 0.1f;

    [Header("Exploding projectiles Camera Shake")]
    [SerializeField] float boomShakeIntensity = 1f;
    [SerializeField] float boomShakeTime = 0.1f;

    [Header("Projectiles hit Camera Shake (Intensity depends on motion value) ")]
    [SerializeField] float projectileHitShakeTime = 0.1f;
    [SerializeField] float shakeProjectileHitMultiplier = 3f;



    protected override void Awake()
    {
        base.Awake();

        cam = GetComponent<CinemachineVirtualCamera>();
    }

    private void ShakeCamera(float intensity, float time)
    {
        CinemachineBasicMultiChannelPerlin CBMCP = cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        CBMCP.m_AmplitudeGain = intensity;
        shakeTimer = time;
    }

    public void CameraShakeOnPlayerHit()
    {
        ShakeCamera(playerHitShakeIntensity, playerHitShakeTime);
    }

    public void CameraShakeOnPlayerDancing()
    {
        ShakeCamera(danceShakeIntensity, danceShakeTime);
    }

    public void CameraShakeOnExplodingProjectile()
    {
        ShakeCamera(boomShakeIntensity, boomShakeTime);
    }

    public void CameraShakeOnProjectileHit(float intensity)
    {
        ShakeCamera(intensity * shakeProjectileHitMultiplier, projectileHitShakeTime);
    }


    protected void Update()
    {
        if (shakeTimer > 0)
        {
            shakeTimer-= Time.deltaTime;
            if (shakeTimer <= 0) 
            {
                CinemachineBasicMultiChannelPerlin CBMCP = cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                CBMCP.m_AmplitudeGain = 0f;
            }
        }

    }
}
