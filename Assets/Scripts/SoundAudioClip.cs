using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundAudioClip
{
    public SoundType type;
    public List<AudioClip> clipVariations = new List<AudioClip>();
}