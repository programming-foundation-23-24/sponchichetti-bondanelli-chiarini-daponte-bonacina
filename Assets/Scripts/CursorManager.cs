using System;
using UnityEngine;

[Serializable]
public class CursorManager
{
    [SerializeField] Texture2D pointerTexture;
    Vector2 pointerHotspot;
    [SerializeField] Texture2D crossHairTexture;
    Vector2 crossHairHotspot;

    public void SetCrossHairAsCursor()
    {
        crossHairHotspot = new Vector2(crossHairTexture.width / 2, crossHairTexture.height / 2);
        Cursor.SetCursor(crossHairTexture, crossHairHotspot, CursorMode.Auto);
    }

    public void SetDefaultPointerAsCursor()
    {
        pointerHotspot = Vector2.zero;
        Cursor.SetCursor(pointerTexture, pointerHotspot, CursorMode.Auto);
    }

    public void HideCursor() { Cursor.visible = false; }

    public void ShowCursor() { Cursor.visible = true; }
}