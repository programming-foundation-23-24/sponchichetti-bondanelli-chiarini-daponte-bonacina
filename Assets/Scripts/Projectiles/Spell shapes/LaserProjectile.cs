using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserProjectile : BaseProjectile
{
    public List<float> durationByLevel = new List<float>();
    public float turnOnTime;

    float timer;
    Vector3 positionOffset;
    [SerializeField] BoxCollider2D laserCollider;
    [SerializeField] Transform spriteTransform;

    float timeBeforeEndingToStartAudioFadeOut = .1f;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
        timer = durationByLevel[projectileLevel];

        positionOffset = travelDirection * (sizeByLevel[projectileLevel].x / 2) + travelDirection * rangeByLevel[projectileLevel];
        Vector3 newPosition = GameManager.Instance.GetPlayerSpellShooterPosition() + positionOffset;
        laserCollider.offset = new Vector2((sizeByLevel[projectileLevel].x / 2) + rangeByLevel[projectileLevel], 0);
        spriteTransform.position = newPosition;

        StartCoroutine(TurnOnLaser());
    }

    protected override void ProjectileBehaviour()
    {
        Vector3 newPosition = GameManager.Instance.GetPlayerSpellShooterPosition() + positionOffset;
        transform.position = GameManager.Instance.GetPlayerSpellShooterPosition();
        spriteTransform.position = newPosition;

        if (timer > 0)
        {
            timer -= Time.deltaTime;
            FadeOutAudioNearEndTime();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    protected override void SetSize()
    {
        //transform.localScale = new Vector2(sizeByLevel[projectileLevel].x, 0.01f);
        laserCollider.size = new Vector2(sizeByLevel[projectileLevel].x, 0.01f);
        spriteTransform.localScale = new Vector2(sizeByLevel[projectileLevel].x, 0.01f);
    }

    IEnumerator TurnOnLaser()
    {
        float elapsedTime = 0;
        while (elapsedTime < turnOnTime)
        {
            laserCollider.size = Vector3.Lerp(spriteTransform.localScale, sizeByLevel[projectileLevel], (elapsedTime / turnOnTime));
            spriteTransform.localScale = Vector3.Lerp(spriteTransform.localScale, sizeByLevel[projectileLevel], (elapsedTime / turnOnTime));
            elapsedTime += Time.deltaTime;

            yield return null;
        }

        spriteTransform.localScale = sizeByLevel[projectileLevel];
        yield return null;
    }

    void FadeOutAudioNearEndTime()
    {
        if (durationByLevel[projectileLevel] < timeBeforeEndingToStartAudioFadeOut)
        {
            float fadeOutValue = Mathf.InverseLerp(timeBeforeEndingToStartAudioFadeOut, 0f, durationByLevel[projectileLevel]);
            audioSource.volume = Mathf.Lerp(baseAudioVolume, 0f, fadeOutValue);
        }
    }
}