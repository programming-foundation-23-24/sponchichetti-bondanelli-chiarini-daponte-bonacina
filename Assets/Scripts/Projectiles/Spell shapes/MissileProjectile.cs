using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileProjectile : BaseProjectile
{
    public BaseProjectile explosionPrefab;

    public List<float> explosionMotionValueByLevel = new List<float>();

    protected override void Explode()
    {
        InstantiateSecondaryProjectile(explosionPrefab, explosionMotionValueByLevel, true);
        base.Explode();
    }
}