using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineProjectile : BaseProjectile
{
    public BaseProjectile explosionPrefab;

    public List<float> explosionMotionValueByLevel = new List<float>();
    public List<int> minesNumberCapByLevel = new List<int>();

    protected override void Awake()
    {
        base.Awake();

        transform.position = GameManager.Instance.GetPlayerPosition();
    }

    protected override void Start()
    {
        base.Start();
        GameManager.Instance.AddMineToList(this, minesNumberCapByLevel[projectileLevel]);
    }

    protected override void ProjectileBehaviour()
    {
        //Statte ferma
    }

    protected override void Explode()
    {
        InstantiateSecondaryProjectile(explosionPrefab, explosionMotionValueByLevel, false);
        base.Explode();
    }

    private void OnDestroy()
    {
        GameManager.Instance.RemoveMineFromList(this);
    }
}