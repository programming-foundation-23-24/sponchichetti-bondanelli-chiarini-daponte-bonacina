using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomauraProjectile : BaseProjectile
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void SetSize()
    {
        transform.localScale = new Vector2(0.1f, 0.1f);
    }
    protected override void Update()
    {
        base.Update();
    }

    protected override void ProjectileBehaviour()
    {
        //con questo metodo la local scale diventa sempre pi� grande della size by level e non di poco, un lerp basato su una durata sarebbe meglio
        if (transform.localScale.x < sizeByLevel[projectileLevel].x)
        {
            float scaleFactor = travelSpeedByLevel[projectileLevel] * Time.deltaTime;

            transform.localScale += new Vector3(scaleFactor, scaleFactor, 0);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    protected override void FadeOutVolumeNearEndRange()
    {
        if (audioSource == null)
        {
            return;
        }

        if (transform.localScale.x > sizeByLevel[projectileLevel].x - distanceBeforeEndingToStartAudioFadeOut && sizeByLevel[projectileLevel].x > transform.localScale.x)
        {
            float fadeOutValue = Mathf.InverseLerp(distanceBeforeEndingToStartAudioFadeOut, 0f, sizeByLevel[projectileLevel].x - transform.localScale.x);
            audioSource.volume = Mathf.Lerp(baseAudioVolume, 0f, fadeOutValue);
        }
    }
}