using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadeProjectile : BaseProjectile
{
    public BaseProjectile explosionPrefab;
    public float explosionTimer = 2;

    public List<float> explosionMotionValueByLevel = new List<float>();

    bool triggered = false;
    float newRange;

    protected override void Awake()
    {
        base.Awake();
        newRange = Vector3.Distance(targetPoint, originPoint);
        rangeByLevel[projectileLevel] = newRange;
    }

    protected override void ProjectileBehaviour()
    {
        if (triggered)
        {
            if (explosionTimer <= 0)
            {
                Explode();
            }
            explosionTimer -= Time.deltaTime;
            return;
        }


        if (distanceTravelled >= newRange)
        {
            triggered = true;
            return;
        }
        
        Vector3 travelDistanceVector = Time.deltaTime * travelSpeedByLevel[projectileLevel] * (Vector3)travelDirection;
        distanceTravelled += travelDistanceVector.magnitude;
        transform.position += travelDistanceVector;
    }

    protected override void Explode()
    {
        InstantiateSecondaryProjectile(explosionPrefab, explosionMotionValueByLevel, false);
        base.Explode();
    }
}