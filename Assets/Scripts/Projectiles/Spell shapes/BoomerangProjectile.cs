using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangProjectile : BaseProjectile
{
    bool reset = false;

    protected override void Awake()
    {
        base.Awake();
    }
    protected override void Update()
    {
        base.Update();
    }

    protected override void ProjectileBehaviour()
    {
        if (distanceTravelled <= rangeByLevel[projectileLevel])
        {
            Vector3 travelDistanceVector = Time.deltaTime * travelSpeedByLevel[projectileLevel] * (Vector3)travelDirection;
            transform.position += travelDistanceVector;
            distanceTravelled += travelDistanceVector.magnitude;
        }
        else if (distanceTravelled > rangeByLevel[projectileLevel])
        {
            ReturnToPlayer();
            if (!reset)
            {
                StartCoroutine(ResetHitList());
                reset = true;
            }
        }
    }

    protected void ReturnToPlayer()
    {
        Vector3 playerPosition = GameManager.Instance.GetPlayerSpellShooterPosition();
        float movementDistance = travelSpeedByLevel[projectileLevel] * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, playerPosition, movementDistance);

        float distanceToPlayer = Vector3.Distance(transform.position, playerPosition);

        if (distanceToPlayer <= 0.1f)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator ResetHitList()
    {
        projectileCollider.enabled = false;
        yield return new WaitForSeconds(.01f);
        projectileCollider.enabled = true;
        alreadyHitEnemies.Clear();
    }
}
