using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpheresProjectile : BaseProjectile
{
    [SerializeField] List<float> durationByLevel = new List<float>();

    Vector2 playerPos;

    Vector2 previousPlayerPos;

    float timeBeforeEndingToStartAudioFadeOut = .4f;

    protected override void Awake()
    {
        base.Awake();

        playerPos = GameManager.Instance.GetPlayerSpellShooterPosition();
        previousPlayerPos = playerPos;
    }

    protected override void Start()
    {
        base.Start();
        SetSpawnDistance();
    }

    protected override void ProjectileBehaviour()
    {
        if (durationByLevel[projectileLevel] <= 0)
        {
            Destroy(gameObject);
        }

        FadeOutAudioNearEndTime();

        durationByLevel[projectileLevel] -= Time.deltaTime;

        playerPos = GameManager.Instance.GetPlayerSpellShooterPosition();

        Vector2 playerMovement = playerPos - previousPlayerPos;

        transform.position += (Vector3) playerMovement;
        transform.RotateAround(playerPos, Vector3.forward, travelSpeedByLevel[projectileLevel] * Time.deltaTime);

        previousPlayerPos = playerPos;
    }

    void SetSpawnDistance()
    {
        Vector2 spawnPosNormalized = ((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - originPoint).normalized;
        transform.position = originPoint + (spawnPosNormalized * rangeByLevel[projectileLevel]);
    }

    public override IEnumerator AddEnemyToHitList(BaseEnemy enemy)
    {
        alreadyHitEnemies.Add(enemy);
        yield return new WaitForSeconds(.2f);
        alreadyHitEnemies.Remove(enemy);
    }

    void FadeOutAudioNearEndTime()
    {
        if (durationByLevel[projectileLevel] < timeBeforeEndingToStartAudioFadeOut)
        {
            float fadeOutValue = Mathf.InverseLerp(timeBeforeEndingToStartAudioFadeOut, 0f, durationByLevel[projectileLevel]);
            audioSource.volume = Mathf.Lerp(baseAudioVolume, 0f, fadeOutValue);
        }
    }
}