using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeProjectile : BaseProjectile
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void SetSize()
    {
        transform.localScale = new Vector2(0.1f, 0.1f);
    }
    protected override void Update()
    {
        base.Update();
    }

    protected override void ProjectileBehaviour()
    {
        Vector3 travelDistanceVector = Time.deltaTime * travelSpeedByLevel[projectileLevel] * (Vector3)travelDirection;
        transform.position += travelDistanceVector;

        distanceTravelled += travelDistanceVector.magnitude;
        if (distanceTravelled >= rangeByLevel[projectileLevel])
        {
            Destroy(gameObject);
        }
        else
        {
            Vector2 scaleFactor = sizeByLevel[projectileLevel] * travelSpeedByLevel[projectileLevel] * Time.deltaTime;
            transform.localScale += (Vector3) scaleFactor;
        }
    }
}
