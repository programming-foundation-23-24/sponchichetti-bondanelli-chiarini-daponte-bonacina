using System.Collections;
using System.Collections.Generic;
using Unity.Properties;
using UnityEngine;


public class ElementalProperty : MonoBehaviour
{
    //Questa classe gestisce ogni cosa che deve cambiare l'elemento
    //ovvero effetto da applicare ai nemici e cambiamento del colore, scusa Bonda se puzza - Spo

    //Effettivamente ho perso 2 minuti confuso a cercare dove lo shooter applicasse l'elemento al proiettile per mettere il suono haha -Bonda

    public Element currentElement = Element.None;

    [SerializeField] bool isSecondaryProjectile;

    private ParticleSystem[] particleSystems;

    public void Awake()
    {
        if (!isSecondaryProjectile)
        {
            CheckPlayerElement();
            SetElementColor();
            PlayElementSound();
        }
    }

    void CheckPlayerElement()
    {
        currentElement = GameManager.Instance.GetPlayerActiveElement();
    }

    public void SetElementColor()
    {
        Color elementColor = GameManager.Instance.elementsColors[(int)currentElement];

        //Cambia colore particelle
        if (GetComponentsInChildren<ParticleSystem>() != null)
        {
            particleSystems = GetComponentsInChildren<ParticleSystem>();
        }

        if (particleSystems.Length > 0)
        {
            foreach (ParticleSystem particleSystem in particleSystems)
            {
                var main = particleSystem.main;
                main.startColor = elementColor;
            }
        }

        //Cambia colore sprite
        if (GetComponent<SpriteRenderer>() != null)
        { 
            GetComponent<SpriteRenderer>().color = elementColor;
        }
        else if (GetComponentInChildren<SpriteRenderer>() != null) 
        {
            GetComponentInChildren<SpriteRenderer>().color = elementColor;
        }
        
    }

    void PlayElementSound()
    {
        SoundManager.Instance.PlaySound(SoundType.FireElement + (int)currentElement - 1, false, .6f);
    }

}