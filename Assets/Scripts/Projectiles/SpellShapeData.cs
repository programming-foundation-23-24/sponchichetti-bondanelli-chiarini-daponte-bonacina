using System;
using UnityEngine;

[Serializable]
public class SpellShapeData
{
    public BaseProjectile projectile;
    public string shapeName;
    public Sprite icon;
}