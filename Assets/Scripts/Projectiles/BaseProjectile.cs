using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    protected Collider2D projectileCollider;

    public int projectileLevel;
    public bool isPiercing = false;

    public List<float> motionValueByLevel = new List<float>();
    public List<float> travelSpeedByLevel = new List<float>();
    public List<float> rangeByLevel = new List<float>();
    public List<Vector2> sizeByLevel = new List<Vector2>();
    protected float distanceTravelled;
    protected Vector2 originPoint;
    protected Vector2 targetPoint;
    protected Vector2 travelDirection;
    //public string targetTag;

    public GameObject onDestroyParticles;
    [SerializeField] bool isSecondary;

    public List<BaseEnemy> alreadyHitEnemies = new List<BaseEnemy>();

    public bool automaticallyFadeOutAudio;
    [SerializeField] protected AudioSource audioSource;
    [Header("Mettere l'audioclip solo se non � gi� presente un'audio source")]
    [SerializeField] AudioClip audioClip;
    [SerializeField] float clipVolume = .7f;
    protected float baseAudioVolume;
    protected float distanceBeforeEndingToStartAudioFadeOut = .5f;

    Element currentElement;

    protected virtual void Awake()
    {
        TryGetComponent(out projectileCollider);

        GetOriginAndTargetPoints();
        CalculateDirectionFromPoints();

        currentElement = GetComponent<ElementalProperty>().currentElement;

    }

    protected virtual void Start()
    {
        SetSize();

        if (TryGetComponent(out audioSource))
        {
            baseAudioVolume = audioSource.volume;
        }
        else
        {
            SoundManager.Instance.PlayAudioClip(audioClip, clipVolume, false);
        }
    }

    protected virtual void SetSize()
    {
        transform.localScale = sizeByLevel[projectileLevel];
    }

    protected virtual void Update()
    {
        ProjectileBehaviour();

        if (automaticallyFadeOutAudio)
        {
            FadeOutVolumeNearEndRange();
        }
    }

    protected virtual void ProjectileBehaviour()
    {
        Vector3 travelDistanceVector = Time.deltaTime * travelSpeedByLevel[projectileLevel] * (Vector3)travelDirection;
        transform.position += travelDistanceVector;

        distanceTravelled += travelDistanceVector.magnitude;
        if (distanceTravelled >= rangeByLevel[projectileLevel])
        {
            Destroy(gameObject);
        }
    }

    protected void SpawnDestroyParticles()
    {
        if (onDestroyParticles != null)
        {
            Instantiate(onDestroyParticles, transform.position, Quaternion.identity);
        }
    }

    protected void GetOriginAndTargetPoints()
    {
        if (!isSecondary)
        {
            targetPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            originPoint = GameManager.Instance.GetPlayerSpellShooterPosition();
        }
    }

    public void CalculateDirectionFromPoints()
    {
        travelDirection = (targetPoint - originPoint).normalized;
        float angle = Mathf.Atan2(travelDirection.y, travelDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void SetNewDirection(Vector2 newDirection)
    {
        travelDirection = newDirection;
        float angle = Mathf.Atan2(travelDirection.y, travelDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void SetNewOriginAndTargetPoints(Vector2 newOriginPoint, Vector2 newTargetPoint)
    {
        originPoint = newOriginPoint;
        targetPoint = newTargetPoint;
    }

    protected void InstantiateSecondaryProjectile(BaseProjectile explosionPrefab, List<float> secondaryMotionValuesByLevel, bool rememberHitEnemies)
    {
        if (explosionPrefab != null)
        {
            BaseProjectile secondaryProjectile = Instantiate(explosionPrefab, transform.position, Quaternion.identity);

            secondaryProjectile.SetNewDirection(travelDirection);
            secondaryProjectile.projectileLevel = projectileLevel;
            secondaryProjectile.motionValueByLevel = secondaryMotionValuesByLevel;

            if (rememberHitEnemies)
            {
                secondaryProjectile.alreadyHitEnemies = alreadyHitEnemies;
            }

            ElementalProperty elegibleSecondaryProjectileElement = secondaryProjectile.GetComponent<ElementalProperty>();
            if (elegibleSecondaryProjectileElement != null)
            {
                ElementalProperty secondaryProjectileElement = elegibleSecondaryProjectileElement;
                secondaryProjectileElement.currentElement = GetComponent<ElementalProperty>().currentElement;
                secondaryProjectileElement.SetElementColor();
            }
            else
            {
                Debug.LogError("Missing ElementalProperty in secondary projectile");
            }
        }
    }

    public void CollisionDetected()
    {
        CameraShake.Instance.CameraShakeOnProjectileHit(motionValueByLevel[projectileLevel]);

        if (isPiercing)
        {
            return;  //in un mondo migliore, se � piercing non avrebbe direttamente il component ContactChecker
        }
        else
        {
            Explode();
        }
    }

    protected virtual void Explode()
    {
        CameraShake.Instance.CameraShakeOnExplodingProjectile();
        SpawnDestroyParticles();
        Destroy(gameObject);
    }

    public virtual IEnumerator AddEnemyToHitList(BaseEnemy enemy)
    {
        alreadyHitEnemies.Add(enemy);
        yield return null;
    }

    protected virtual void FadeOutVolumeNearEndRange()
    {
        if(audioSource == null)
        {
            return;
        }

        if (distanceTravelled > rangeByLevel[projectileLevel] - distanceBeforeEndingToStartAudioFadeOut)
        {
            float fadeOutValue = Mathf.InverseLerp(distanceBeforeEndingToStartAudioFadeOut, 0f, rangeByLevel[projectileLevel] - distanceTravelled);
            audioSource.volume = Mathf.Lerp(baseAudioVolume, 0f, fadeOutValue);
        }
    }
}