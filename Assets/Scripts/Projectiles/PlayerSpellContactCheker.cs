﻿using UnityEngine;

public class PlayerSpellContactCheker : BaseContactChecker
{
    BaseProjectile baseProjectile;

    private void Awake()
    {
        TryGetComponent(out baseProjectile);
    }

    protected override void OnCollisionContactEnter(Collision2D otherCollision)
    {
    }

    protected override void OnTriggerContactEnter(Collider2D otherCollision)
    {
        if (otherCollision.GetComponent<BaseEnemy>() != null)
        {
            baseProjectile.CollisionDetected();
        }
    }
}