﻿using System;
using UnityEngine;

[Serializable]
public class PlayerSlot
{
    public DanceInput danceInput;
    public BaseProjectile projectile;
    public bool locked;
    public int shapeLevel;
    public Sprite shapeIcon;
    public string shapeName;
}