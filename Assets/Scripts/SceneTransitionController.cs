using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransitionController : Singleton<SceneTransitionController>
{
    public float TransitionTime = 1f;

    [SerializeField] Animator transitionAnimator;


    protected override void Awake()
    {
        base.Awake();
    }
    public void StartSceneTransition()
    {
        if (transitionAnimator != null)
            transitionAnimator.SetTrigger("TriggerTransition");
        else
            Debug.Log("No transition animator referenced");
        
    }

    public void LoadSelectedScene(string sceneName)
    {
        StartCoroutine(LoadSelectedSceneCoroutine(sceneName));
    }

    public IEnumerator LoadSelectedSceneCoroutine(string sceneName) 
    {
        StartSceneTransition();

        if (GameScene())
        {
            SoundManager.Instance.StopLevelMusic();
        }

        Debug.Log("Loading");
        yield return new WaitForSecondsRealtime(TransitionTime);
        Debug.Log("Loaded");
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        if (GameScene())
        {
            GameManager.Instance.ResetGameTime();
        }

        SceneManager.LoadScene(sceneName);
    }

    bool GameScene()
    {
        return GameManager.Instance != null;
    }
}
