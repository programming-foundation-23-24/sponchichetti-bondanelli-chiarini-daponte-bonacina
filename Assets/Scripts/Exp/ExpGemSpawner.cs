using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpGemSpawner : MonoBehaviour
{
    public GameObject GemPrefab;
    public float spawnRadius;

    /*void Update()
    {
        TESTDropExpGems(1);
    }
    public void TESTDropExpGems(int gems)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            for (int i = 0; i < gems; i++)
            {
                Vector3 randomOffset = Random.insideUnitSphere * spawnRadius;
                Vector3 randomPosition = transform.position + randomOffset;
                Instantiate(GemPrefab, randomPosition, Quaternion.identity);
            }
        }     
    }*/

    public void DropExpGems(int numberOfDrops)
    {
        if (numberOfDrops == 1)
        {
            Instantiate(GemPrefab, transform.position, Quaternion.identity);
            return;
        }

        for (int i = 0; i < numberOfDrops; i++)
        {
            Vector3 randomOffset = Random.insideUnitSphere * spawnRadius;
            Vector3 randomPosition = transform.position + randomOffset;
            Instantiate(GemPrefab, randomPosition, Quaternion.identity);
        }    
    }
}
