using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MagneticProperty : MonoBehaviour
{
    [SerializeField] float magnetismSpeed;

    Vector2 playerPosition;

    void Update()
    {
        playerPosition = GameManager.Instance.GetPlayerPosition();
        if (GameManager.Instance.expMagnetismRange > Vector3.Distance(transform.position, playerPosition))
        {
            GetAttractedToPlayer();
        }
    }

    public void GetAttractedToPlayer()
    {
        Vector2 newPosition = Vector2.MoveTowards(transform.position, playerPosition, Time.deltaTime * magnetismSpeed/ Vector3.Distance(transform.position, playerPosition));

        transform.position = newPosition;
    }
}