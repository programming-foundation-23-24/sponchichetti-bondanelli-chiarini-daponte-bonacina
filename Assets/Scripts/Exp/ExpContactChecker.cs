using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpContactChecker : BaseContactChecker
{
    ExpGem expGem;

    void Start()
    {
        TryGetComponent(out expGem);
    }
    protected override void OnCollisionContactEnter(Collision2D otherCollision)
    {

    }

    protected override void OnTriggerContactEnter(Collider2D otherCollision)
    {
        if (otherCollision.CompareTag("Player"))
        {
            GameManager.Instance.PickUpGem(expGem.expValue);
            Destroy(gameObject);
        }
    }
}