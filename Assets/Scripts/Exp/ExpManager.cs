using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ExpManager
{
    //y = a(b^x) + (x * c)

    //y � exp che mi servono per livellare
    //a,b e c sono coefficenti arbitrari
    //x � il livello

    [SerializeField] PlayerSpellShapesDatabase spellShapesDatabase;

    public int currentLevel = 1;
    public int maxLevel = 12;

    public int currentExp;
    [SerializeField] int expToLevelUp;

    [Header("expToLevelUp = a (b^currentLevel) + (x * c)")]
    public float a = 3f;
    public float b = 1.6f; // can't be 1
    public float c = 8f;

    public bool levelUpRequestCompleted;

    public void AddExp(int gemValue)
    {
        currentExp += gemValue;
        SoundManager.Instance.PlaySound(SoundType.ExpPickUp, false, .8f);
        GameManager.Instance.UpdateExpUI(currentExp, expToLevelUp, currentLevel, maxLevel);
        if(CheckLevelUp())
        {
            GameManager.Instance.StartCoroutine(LevelUp());
        }
    }

    public int CalculateExpToLevelUp()
    {
        expToLevelUp = (int) (a * (Mathf.Pow(b, (float) currentLevel)) + (currentLevel * c));    
        return expToLevelUp;
    }

    public bool CheckLevelUp()
    {
        if (currentExp >= expToLevelUp && currentLevel < maxLevel)
        {
            return true;
        }
        return false;
    }

    public IEnumerator LevelUp()
    {
        GameManager.Instance.cursorManager.SetDefaultPointerAsCursor();

        GameManager.Instance.PausePlayTime();

        SoundManager.Instance.PlaySound(SoundType.LevelUp, true);

        currentExp -= expToLevelUp;
        currentLevel++;
        CalculateExpToLevelUp();
        GameManager.Instance.UpdateExpUI(currentExp, expToLevelUp, currentLevel, maxLevel);

        if (GameManager.Instance.playerSpellShapesDatabase.AllShapesSlotsAreLockedIn())
        {
            GameManager.Instance.playerSpellShapesDatabase.UpgradeSelectionPhase();
        }
        else
        {
            GameManager.Instance.playerSpellShapesDatabase.ShapeSelectionPhase();
        }

        yield return new WaitUntil(() => levelUpRequestCompleted);
        levelUpRequestCompleted = false;

        if (CheckLevelUp())
        {
            GameManager.Instance.StartCoroutine(LevelUp());
            yield return null;
        }
        else
        {
            GameManager.Instance.cursorManager.SetCrossHairAsCursor();
            GameManager.Instance.ResumePlayTime();
            yield return null;
        }
    }
}
