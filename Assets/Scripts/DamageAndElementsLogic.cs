using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

[Serializable]
public class DamageAndElementsLogic
{
    public float playerBaseAttackValue;

    public DamageNumber damageNumberPrefab;

    [Header("Fire DOT")]
    public float fireDamagePercentagePerTick;
    public int fireNumberOfTicks;
    public float fireDuration;

    [Header("Ice freeze (Max * MV)")]
    public float iceFreezeMaximumDuration;

    [Header("Earth knockback")]
    public float earthMaxKnockbackDistance;
    public float earthMaxKnockbackDuration;

    [Header("Thunderous speed")]
    public float thunderSpeedIncrementMultiplier;
    public float thunderSpeedBoostDuration;
    [Range (0,7)] public float actualSpeedBoost;
    public float maxSpeedBoost;

    public void DealDamageToPlayer(int damage, PlayerHealth playerHealth)
    {
        CameraShake.Instance.CameraShakeOnPlayerHit();

        playerHealth.currentHealth -= damage;
        GameManager.Instance.UpdateHealthUI();

        if (playerHealth.CheckIfDead()) return;

        playerHealth.StartCoroutine(playerHealth.PlayerGotHitVFX());
        playerHealth.StartCoroutine(playerHealth.ResetDamageBuffer());
    }

    public void HealPlayer(int value)
    {
        PlayerHealth playerHealth = GameManager.Instance.playerHealth;
        if (playerHealth.currentHealth < playerHealth.maxHealth)
        {
            playerHealth.currentHealth += value;
            GameManager.Instance.UpdateHealthUI();
            SoundManager.Instance.PlaySound(SoundType.Heal, false, .9f);
        }
    }

    public void DealProjectileDamageAndElementToEnemy(BaseEnemy enemy, BaseProjectile projectile)
    {
        float motionValue = projectile.motionValueByLevel[projectile.projectileLevel];

        if (motionValue <= 0)   //Mina
        {
            return;
        }

        DealDamageToEnemy(motionValue * playerBaseAttackValue, enemy);

        SoundManager.Instance.PlaySound(SoundType.EnemyHit, false, .65f);

        ElementalProperty elementalProperty = projectile.GetComponent<ElementalProperty>();

        ApplyElementEffectToEnemy(elementalProperty, motionValue, projectile.transform.position, enemy);
    }

    public void DealDamageToEnemy(float damage, BaseEnemy enemy)
    {
        if (damage >= 1)
        {
            GameManager.Instance.ShowDamageNumber(enemy, (int)damage);
        }

        enemy.healthPoints -= damage;

        if (enemy.healthPoints <= 0)
        {

            enemy.IsDead();
            return;
        }

        //GameManager.Instance.StartCoroutine(ResetEnemyDamageBuffer(enemy));
        enemy.StartCoroutine(enemy.HitVFX());
    }

    //outdated ma non si sa mai
    /*IEnumerator ResetEnemyDamageBuffer(BaseEnemy enemy)
    {
        enemy.invicibility = true;
        yield return new WaitForSeconds(enemy.damageBufferDuration);
        enemy.invicibility = false;
    }*/

    void ApplyElementEffectToEnemy(ElementalProperty elementalProperty, float motionValue, Vector3 projectilePosition, BaseEnemy enemy)
    {
        if (elementalProperty != null)
        {
            switch (elementalProperty.currentElement)
            {
                case Element.Fire:
                    ApplyFireToEnemy(motionValue, enemy);
                    break;
                case Element.Thunder:
                    ApplyThunderToPlayer(motionValue);
                    break;
                case Element.Ice:
                    ApplyIceToEnemy(motionValue, enemy);
                    break;
                case Element.Earth:
                    ApplyEarthToEnemy(motionValue, projectilePosition, enemy);
                    break;
            }
        }
    }

    void ApplyFireToEnemy(float motionValue, BaseEnemy enemy)
    {
        if (enemy.isFreezed)
        {
            enemy.StopFreeze();
        }

        if (!enemy.isBurning)
        {
            enemy.elementalStatusCoroutine = enemy.StartCoroutine(FireDOT(enemy, motionValue * playerBaseAttackValue * (fireDamagePercentagePerTick / 100), fireDuration, fireNumberOfTicks));
        }
        else
        {
            enemy.StopFire();
            enemy.elementalStatusCoroutine = enemy.StartCoroutine(FireDOT(enemy, motionValue * playerBaseAttackValue * (fireDamagePercentagePerTick / 100), fireDuration, fireNumberOfTicks));
        }
    }

    IEnumerator FireDOT(BaseEnemy enemy, float damage, float DOTduration, int numberOfTicks)
    {
        enemy.SetElementalStatus(Element.Fire);

        float numberOfRemainingTicks = numberOfTicks;

        float timerDOT = DOTduration / numberOfTicks;

        while (numberOfRemainingTicks > 0)
        {
            if (!enemy.isBurning) break;

            if (timerDOT <= 0)
            {
                DealDamageToEnemy(damage, enemy);
                timerDOT = DOTduration / numberOfTicks;
                numberOfRemainingTicks -= 1;
            }

            timerDOT -= Time.deltaTime;

            yield return null;
        }

        yield return new WaitForSeconds(.1f);

        enemy.SetElementalStatus(Element.None);

        yield return null;
    }

    void ApplyIceToEnemy(float motionValue, BaseEnemy enemy)
    {
        if (enemy.isBurning)
        {
            enemy.StopFire();
        }

        if (!enemy.isFreezed)
        {
            enemy.elementalStatusCoroutine = enemy.StartCoroutine(IceFreeze(enemy, iceFreezeMaximumDuration * motionValue));
        }
        else
        {
            enemy.freezedTimer = 0;
        }
    }

    IEnumerator IceFreeze(BaseEnemy enemy, float freezeDuration)
    {
        enemy.SetElementalStatus(Element.Ice);
        enemy.freezedTimer = 0;

        float normalMovementSpeed = enemy.movementSpeed;
        enemy.SetMovementSpeed(0);
        
        while (enemy.freezedTimer < freezeDuration)
        {
            enemy.freezedTimer += Time.deltaTime;

            yield return null;
        }

        enemy.SetMovementSpeed(normalMovementSpeed);
        enemy.SetElementalStatus(Element.None);

        yield return null;
    }

    void ApplyEarthToEnemy(float motionValue, Vector3 projectilePosition, BaseEnemy enemy)
    {
        enemy.StartCoroutine(enemy.Knockback(projectilePosition, earthMaxKnockbackDistance * motionValue, earthMaxKnockbackDuration * motionValue)); //formula provvisoria
    }

    void ApplyThunderToPlayer(float motionValue)
    {
        GameManager.Instance.playerController.StartCoroutine(ThunderSpeedBoost(thunderSpeedIncrementMultiplier * motionValue, thunderSpeedBoostDuration));
    }

    IEnumerator ThunderSpeedBoost(float speedIncrement, float speedBoostDuration)
    {
        if (actualSpeedBoost + speedIncrement > maxSpeedBoost)
        {
            speedIncrement = maxSpeedBoost - actualSpeedBoost;
        }

        actualSpeedBoost += speedIncrement;

        yield return new WaitForSeconds(speedBoostDuration);

        actualSpeedBoost -= speedIncrement;

        if (actualSpeedBoost < 0.05f)
        {
            actualSpeedBoost = 0f;
        }
    }
}