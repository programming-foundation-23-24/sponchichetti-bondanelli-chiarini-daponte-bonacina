using UnityEngine;

public class ParticlesController : MonoBehaviour
{
    [SerializeField] ParticleSystem _particleSystem;

    void Awake()
    {
        if (_particleSystem != null)
        { 
             _particleSystem = GetComponent<ParticleSystem>();
            var main = _particleSystem.main;
            Destroy(gameObject, main.duration + 1);
        }
        else
        {
            Destroy(gameObject, 5f);
        }
        
    }
}