using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CutsceneManager : MonoBehaviour
{
    public List<CutsceneData> cutscenes = new List<CutsceneData> ();

    void Start()
    {
        StartCutscene();
    }

    private void StartCutscene()
    {
        Debug.Log("Cutscene Manager start");
        StartCoroutine(StartActing());
        SoundManager.Instance.PlaySoundtrack(SoundType.ST_cutscene, .1f);
    }

    public void EndCutscene()
    {
        SoundManager.Instance.StopSoundtrack();
    }

    IEnumerator StartActing()
    {
        Debug.Log("start acting");
        foreach (CutsceneData cutscene in cutscenes)
        {
            Debug.Log("cutscene");

            foreach (GameObject obj in cutscene.objectsToActivateBeforeActingTime)
            {
                obj.SetActive(true);
            }
            foreach (GameObject obj in cutscene.objectsToActivateAndThenDeactivateForActingTime)
            {
                obj.SetActive(true);
            }
            foreach (GameObject obj in cutscene.objectsToDeactivateBeforeActingTime)
            {
                obj.SetActive(false);
            }
            foreach (UnityEvent ue in cutscene.UnityEventsInvokedAtStartOfCutscene)
            {
                ue.Invoke ();
            }

            yield return new WaitForSeconds(cutscene.actingTime);

            Debug.Log("waited acting time");

            foreach (GameObject obj in cutscene.objectsToActivateAndThenDeactivateForActingTime)
            {
                obj.SetActive(false);
            }
        }
    }
}
