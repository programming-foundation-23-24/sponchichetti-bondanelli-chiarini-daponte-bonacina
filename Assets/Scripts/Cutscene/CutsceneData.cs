using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[Serializable]
public class CutsceneData
{
    public string cutsceneName;
    public float actingTime;
    public List<GameObject> objectsToActivateBeforeActingTime;
    public List<GameObject> objectsToDeactivateBeforeActingTime;
    public List<GameObject> objectsToActivateAndThenDeactivateForActingTime;
    public List<UnityEvent> UnityEventsInvokedAtStartOfCutscene;
}
