using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleAnimationButton : BaseButton
{
    [SerializeField] Animator animator;
    [SerializeField] string animTrigger1;
    [SerializeField] string animTrigger2;
    public bool buttonClicked;

    protected override void OnClick()
    {
        if (!buttonClicked) 
        {
            buttonClicked = true;
            animator.SetTrigger(animTrigger1);
        }
        else
        {
            buttonClicked = false;
            animator.SetTrigger(animTrigger2);
        }
    }
}
