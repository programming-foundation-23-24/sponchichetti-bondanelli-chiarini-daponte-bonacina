using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LoadSceneButton : BaseButton
{
    public string sceneToLoad;

    protected override void OnClick()
    {
        base.OnClick();
        SceneTransitionController.Instance.LoadSelectedScene(sceneToLoad);
    }
}
