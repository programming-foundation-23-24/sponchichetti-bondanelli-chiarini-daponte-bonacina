using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubMenuButton : BaseButton
{
    [Header("GameObject to activate on click")]
    public List<GameObject> thingsToSetActive;
    [Header("GameObject to disactivate on click")]
    public List<GameObject> thingsToSetInactive;
    //andrebbe fatto con subscribe pattern
    protected override void OnClick()
    {
        base.OnClick();
        //Attivare
        foreach (GameObject go in thingsToSetActive)
        {
            go.SetActive(true);
        }

        //Disattivare
        foreach (GameObject go in thingsToSetInactive)
        {
            go.SetActive(false);
        }
    }
}
