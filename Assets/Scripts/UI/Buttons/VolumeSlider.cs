using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour, ISelectHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] Slider slider;
    [SerializeField] string targetMixerVolumeName;

    bool muteSelectionSound;

    private void Awake()
    {
        if (slider == null)
        {
            GetComponent<Slider>();
        }

        slider.onValueChanged.AddListener(OnValueChanged);
    }

    private void Start()
    {
        switch (targetMixerVolumeName)
        {
            case "masterVolume":
                slider.value = SoundManager.Instance.masterVolume;
                break;
            case "musicVolume":
                slider.value = SoundManager.Instance.musicVolume;
                break;
            case "sfxVolume":
                slider.value = SoundManager.Instance.sfxVolume;
                break;
            default:
                Debug.LogError($"This slider ({this}) is aiming at a non existent Mixer");
                return;
        }
    }

    void OnValueChanged(float sliderValue)
    {
        SoundManager.Instance.ChangeMixerVolume(targetMixerVolumeName, Mathf.Log(sliderValue) * 20);
        //uso mathf.log perch� lo slider � lineare mentre il volume logaritmico, sarebbe Log10(x�) (segnale di voltaggio) = lg(x) * 20
        //data questa formula, lo slider va settato con valore minimo 0.001 e valore massimo 1
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (!muteSelectionSound)
        {
            SoundManager.Instance.PlaySound(SoundType.UI_Select, true, .5f);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        muteSelectionSound = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        muteSelectionSound = false;
    }
}
