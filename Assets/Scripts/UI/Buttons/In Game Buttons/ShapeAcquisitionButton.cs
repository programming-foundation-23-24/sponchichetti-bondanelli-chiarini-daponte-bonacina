﻿using UnityEngine;

public class ShapeAcquisitionButton : BaseButton
{
    public SpellShapeData shapeData;

    protected override void OnClick()
    {
        if (shapeData != null)
        {
            GameManager.Instance.SelectedShapeToAcquire(shapeData);
            base.OnClick();
        }
        else
        {
            Debug.LogError($"{this} shape data is missing");
        }
    }

    public void UpdateVisualToShapeData()
    {
        button.image.sprite = shapeData.icon;
        tMPro.text = shapeData.shapeName;
    }
}