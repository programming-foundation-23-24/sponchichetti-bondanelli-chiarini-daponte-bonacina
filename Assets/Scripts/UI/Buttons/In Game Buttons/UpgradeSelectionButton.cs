﻿public class UpgradeSelectionButton : BaseButton
{
    public DanceInput relatedDanceInput;

    protected override void OnClick()
    {
        GameManager.Instance.SelectedSlotToUpgrade(relatedDanceInput);
        base.OnClick();
    }
}