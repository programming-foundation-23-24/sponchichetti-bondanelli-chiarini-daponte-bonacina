﻿using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShapeAllocationButton : BaseButton
{
    public DanceInput relatedDanceInput;

    public Sprite shapeToBeAllocatedSprite;
    Sprite slotShapeSprite;

    public bool slotLocked;

    private void Start()
    {
        slotShapeSprite = button.image.sprite;
    }

    protected override void OnClick()
    {
        if (!slotLocked)
        {
            GameManager.Instance.SelectedSlotForShape(relatedDanceInput);
            base.OnClick();
        }
        else
        {
            SoundManager.Instance.PlaySound(SoundType.UI_Blocked, true, .5f);
            StartCoroutine(BlockedVFX());
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        //se slot non bloccato, immagine diventa quella della spell da allocare

        if (!slotLocked)
        {
            button.image.sprite = shapeToBeAllocatedSprite;
        }
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        //immagine torna dardo base

        if (!slotLocked)
        {
            button.image.sprite = slotShapeSprite;
        }
    }

    IEnumerator BlockedVFX()
    {
        button.image.color = Color.red;

        yield return new WaitForSecondsRealtime(.1f);

        button.image.color = Color.white;

        yield return new WaitForSecondsRealtime(.05f);

        button.image.color = Color.red;

        yield return new WaitForSecondsRealtime(.1f);

        button.image.color = Color.white;
    }
}