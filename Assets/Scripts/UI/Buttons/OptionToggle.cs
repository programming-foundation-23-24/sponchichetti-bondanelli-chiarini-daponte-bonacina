using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OptionToggle : MonoBehaviour, ISelectHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Toggle toggle;
    public UnityEvent<bool> onValueChanged;

    bool muteSelectionSound = false;

    protected virtual void Awake()
    {
        if (toggle == null)
        {
            toggle = GetComponent<Toggle>();
        }

        toggle.onValueChanged.AddListener(OnValueChange);
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (!muteSelectionSound)
        {
            SoundManager.Instance.PlaySound(SoundType.UI_Select, true, .5f);
        }
    }

    protected virtual void OnDestroy()
    {
        toggle.onValueChanged.RemoveListener(OnValueChange);
    }

    protected void OnValueChange(bool newValue)
    {
        onValueChanged.Invoke(newValue);
        SoundManager.Instance.PlaySound(SoundType.UI_Confirm, true, .5f);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        muteSelectionSound = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        muteSelectionSound = false;
    }
}