using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class BaseButton : MonoBehaviour, ISelectHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Button button;
    public TextMeshProUGUI tMPro;
    SoundType soundOnClick = SoundType.UI_Confirm;

    bool muteSelectionSound = false;

    protected virtual void Awake()
    {
        button.onClick.AddListener(OnClick);

        if (button == null) 
        {
            button = GetComponent<Button>();
        }

        if (tMPro == null)
        {
            tMPro = GetComponentInChildren<TextMeshProUGUI>();
        }

        StartCoroutine(antiSelectionSoundOnStartRoutine());
    }

    public virtual void OnSelect(BaseEventData eventData)
    {
        if (!muteSelectionSound)
        {
            SoundManager.Instance.PlaySound(SoundType.UI_Select, true, .5f);
        }
    }

    protected virtual void OnDestroy()
    {
        button.onClick.RemoveListener(OnClick);
    }

    protected virtual void OnClick()
    {
        SoundManager.Instance.PlaySound(soundOnClick, true, .5f);
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        muteSelectionSound = true;
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        muteSelectionSound = false;
    }

    IEnumerator antiSelectionSoundOnStartRoutine()
    {
        muteSelectionSound = true;
        yield return new WaitForSecondsRealtime(.1f);
        muteSelectionSound = false;
    }
}