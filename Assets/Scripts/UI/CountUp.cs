using UnityEngine;

public class CountUp : MonoBehaviour
{
    public void PlayLowBeep()
    {
        SoundManager.Instance.PlaySoundDelayed(SoundType.UI_LowBeep, 0, .45f);
    }

    public void PlayHighBeep()
    {
        SoundManager.Instance.PlaySoundDelayed(SoundType.UI_HighBeep, 0, .5f);
    }

    public void CountEnd()
    {
        GameManager.Instance.StartGame();
    }
}