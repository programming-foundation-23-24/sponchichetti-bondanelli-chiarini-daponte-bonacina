using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    [SerializeField] Toggle coreomancyOrderToggle;
    [SerializeField] Toggle damageNumbersToggle;

    public void ToggleInvertOrderCoreomancyActivated()
    {
        GameManager.Instance.InvertCoreomancyStepsOrder();
    }

    public void ToggleDamageNumbersActivated()
    {
        GameManager.Instance.ToggleDamageNumbers();
    }
}