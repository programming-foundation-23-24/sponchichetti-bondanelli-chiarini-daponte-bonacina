using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICharacterController : MonoBehaviour
{
    public float moveDistance = 10f;
    public float lerpSpeed = 10f;
    private Vector2 initialPosition;
    private Vector2 targetPosition;
    public Animator animator;
    float previousHorizontalInput = 0f;
    float previousVerticalInput = 0f;

    void Start()
    {
        initialPosition = GetComponent<RectTransform>().anchoredPosition;
    }

    void Update()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");
        Vector2 moveDirection = new Vector2(horizontalInput, verticalInput).normalized;

        if (Mathf.Abs(moveDirection.x) > Mathf.Abs(moveDirection.y))
        {
            moveDirection.y = 0;
        }
        else
        {
            moveDirection.x = 0;
        }

        targetPosition = initialPosition + moveDirection * moveDistance;
        GetComponent<RectTransform>().anchoredPosition = targetPosition;

        if (horizontalInput != previousHorizontalInput)
        {
            if (horizontalInput < 0)
                animator.SetTrigger("Left");
            else if (horizontalInput > 0)
                animator.SetTrigger("Right");
        }

        if (verticalInput != previousVerticalInput)
        {
            if (verticalInput < 0)
                animator.SetTrigger("Down");
            else if (verticalInput > 0)
                animator.SetTrigger("Up");
        }

        // Update previous input state
        previousHorizontalInput = horizontalInput;
        previousVerticalInput = verticalInput;
    }
}

