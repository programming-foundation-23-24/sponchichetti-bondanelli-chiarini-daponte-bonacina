using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AvoidNoButtonSelected : MonoBehaviour
{
    private EventSystem eventSystem;
    private Selectable lastSelectedButton;
    private GameObject lastSelectedGO;

    void Start()
    {
        eventSystem = EventSystem.current;
    }

    void Update()
    {
        if (eventSystem.currentSelectedGameObject == null)
        {
            if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
            {
                float verticalAxis = Input.GetAxisRaw("Vertical");
                float horizontalAxis = Input.GetAxisRaw("Horizontal");

                lastSelectedGO.TryGetComponent(out lastSelectedButton);

                if (verticalAxis > 0)
                {
                    Selectable eligibleNextSelection = lastSelectedButton.FindSelectableOnUp();

                    if (eligibleNextSelection != null)
                    {
                        eventSystem.SetSelectedGameObject(eligibleNextSelection.gameObject);
                    }
                    else
                    {
                        eventSystem.SetSelectedGameObject(lastSelectedGO.gameObject);
                    }
                }
                else if (verticalAxis < 0)
                {
                    Selectable eligibleNextSelection = lastSelectedButton.FindSelectableOnDown();

                    if (eligibleNextSelection != null)
                    {
                        eventSystem.SetSelectedGameObject(eligibleNextSelection.gameObject);
                    }
                    else
                    {
                        eventSystem.SetSelectedGameObject(lastSelectedGO.gameObject);
                    }
                }
                else if (horizontalAxis > 0)
                {
                    Selectable eligibleNextSelection = lastSelectedButton.FindSelectableOnRight();

                    if (eligibleNextSelection != null)
                    {
                        eventSystem.SetSelectedGameObject(eligibleNextSelection.gameObject);
                    }
                    else
                    {
                        eventSystem.SetSelectedGameObject(lastSelectedGO.gameObject);
                    }
                }
                else if (horizontalAxis < 0)
                {
                    Selectable eligibleNextSelection = lastSelectedButton.FindSelectableOnLeft();

                    if (eligibleNextSelection != null)
                    {
                        eventSystem.SetSelectedGameObject(eligibleNextSelection.gameObject);
                    }
                    else
                    {
                        eventSystem.SetSelectedGameObject(lastSelectedGO.gameObject);
                    }
                }
            }
        }
        else
        {
            lastSelectedGO = eventSystem.currentSelectedGameObject;
        }
    }
}