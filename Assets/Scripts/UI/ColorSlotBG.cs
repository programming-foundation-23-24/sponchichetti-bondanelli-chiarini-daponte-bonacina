using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSlotBG : MonoBehaviour
{
    Image image;

    [Range(1, 4)] public int colorIndex;
    private void Start()
    {
        image = GetComponent<Image>();
        image.color = GameManager.Instance.elementsColors[colorIndex];
    }
}
