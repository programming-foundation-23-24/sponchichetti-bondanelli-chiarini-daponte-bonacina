using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DamageNumber : MonoBehaviour
{
    TextMeshPro _textMeshPro;

    public void ShowMe(int damage)
    {
        _textMeshPro = GetComponent<TextMeshPro>();
        _textMeshPro.text = damage.ToString();
        Destroy(gameObject, .7f);
    }

}