using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreomancyUI : MonoBehaviour
{
    [SerializeField] GameObject slowMoFilter;

    public void TurnOnSlowMoVFX()
    {
        slowMoFilter.SetActive(true);
    }

    public void TurnOffSlowMoVFX()
    {
        slowMoFilter.SetActive(false);
    }
}