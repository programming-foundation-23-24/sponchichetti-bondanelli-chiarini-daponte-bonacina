using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthUI : MonoBehaviour
{
    public Image[] Hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    private void Start()
    {
        HearthsUpdater();
    }
    public void HearthsUpdater()
    {
        int UIcurrentHearts = GameManager.Instance.GetPlayerCurrentHealth();
        int UImaxHearts = GameManager.Instance.GetPlayerMaxHealth();

        for (int i = 0; i < Hearts.Length; i++)
        {
            //Riempire i cuori vuoti
            if (i < UIcurrentHearts)
            {
                Hearts[i].sprite = fullHeart;
            }
            else
            {
                Hearts[i].sprite = emptyHeart;
            }

            //Settare massimi cuori vuoti
            if (i < UImaxHearts)
            {
                Hearts[i].enabled = true;
            }
            else
            {
                Hearts[i].enabled = false;
            }
        }
    }
}

