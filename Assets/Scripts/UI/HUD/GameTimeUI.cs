using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameTimeUI : MonoBehaviour
{
    public TextMeshProUGUI textBox;

    // Update is called once per frame
    void Update()
    {
        int totalElapsedSeconds = (int)GameManager.Instance.currentGameTime;
        //int elapsedMinutes = totalElapsedSeconds / 60;
        //int remainingElapsedSeconds = totalElapsedSeconds - (elapsedMinutes * 60);

        int totalSecondsRemaining = 600 - totalElapsedSeconds;
        int remainingMinutes = totalSecondsRemaining / 60;
        int remainingSeconds = totalSecondsRemaining % 60;

        if (totalSecondsRemaining <= 0)
        {
            textBox.text = $"00 : 00";
        }
        else
        {
            if(remainingSeconds >= 10)
            {
                textBox.text = "" + remainingMinutes.ToString() + " : " + remainingSeconds.ToString();
            }
            else
            {
                textBox.text = "" + remainingMinutes.ToString() + " : 0" + remainingSeconds.ToString();
            }
        }

        /*
        if (elapsedMinutes >= 10)
        {
            textBox.text = $"10 : 00";
        }
        else
        {
            
            if (remainingElapsedSeconds < 10)
            {
                textBox.text = $"" + elapsedMinutes.ToString() + " : 0" + (totalElapsedSeconds - (elapsedMinutes * 60)).ToString();
            }
            else
            {
                textBox.text = $"" + elapsedMinutes.ToString() + " : " + (totalElapsedSeconds - (elapsedMinutes * 60)).ToString();
            }*/
    }


}
