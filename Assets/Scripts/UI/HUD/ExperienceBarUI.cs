using UnityEngine;
using UnityEngine.UI;

public class ExperienceBarUI : MonoBehaviour
{
    [SerializeField] Image expBar;
    [SerializeField] GameObject maxLevelText;

    public void UpdateExpBar(float actualExp, float ExpForNextLevel, int currentLevel, int maxLevel)
    {
        if (currentLevel < maxLevel)
        {
            expBar.fillAmount = actualExp / ExpForNextLevel;
        }
        else
        {
            expBar.fillAmount = 1;
            maxLevelText.SetActive(true);
        }
    }
}