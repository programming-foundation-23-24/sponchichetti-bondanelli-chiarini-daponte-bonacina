using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SlotUI : MonoBehaviour
{
    [SerializeField] DanceInput danceInput;
    Image image;
    TextMeshProUGUI textMeshPro;

    private void Awake()
    {
        image = GetComponent<Image>();
        textMeshPro = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Start()
    {
        GameManager.Instance.OnLevelUpDone += SetImage;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnLevelUpDone -= SetImage;
    }

    public void SetImage()
    {
        foreach (PlayerSlot slot in PlayerSpellShapesDatabase.Instance.playerSlots)
        {
            if (slot.danceInput == danceInput)
            {
                image.sprite = slot.shapeIcon;
                if (slot.shapeLevel > 0)
                {
                    textMeshPro.text = (slot.shapeLevel + 1).ToString();
                }
            }
        }
    }
}