using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpUI : MonoBehaviour
{
    PlayerSpellShapesDatabase shapeDatabase;
    GameManager gameManager;

    [SerializeField] GameObject shapeSelectionPanel;
    [SerializeField] List<ShapeAcquisitionButton> buttonsForShapeAcquisition = new List<ShapeAcquisitionButton>();
    [SerializeField] GameObject shapeAllocationPanel;
    [SerializeField] List<ShapeAllocationButton> buttonsForShapeAllocation = new List<ShapeAllocationButton>();

    [SerializeField] GameObject upgradeSelectionPanel;
    [SerializeField] List<UpgradeSelectionButton> buttonsForUpgradeSelection = new List<UpgradeSelectionButton>();


    void Start()
    {
        shapeDatabase = PlayerSpellShapesDatabase.Instance;
        gameManager = GameManager.Instance;

        shapeDatabase.OnShapesChosenForLvlUp += ShowChosenShapesForLevelUp;
        shapeDatabase.OnSlotsChosenForLvlUp += ShowChosenUpgradesForLevelUp;

        gameManager.OnShapeToBeAcquiredSelected += StartShapeAllocationScreen;
        gameManager.OnSlotForShapeSelected += EndShapeAcquisitionScreen;
        gameManager.OnSlotToUpgradeSelected += EndUpgradeScreen;
    }

    void OnDestroy()
    {
        shapeDatabase.OnShapesChosenForLvlUp -= ShowChosenShapesForLevelUp;
        shapeDatabase.OnSlotsChosenForLvlUp -= ShowChosenUpgradesForLevelUp;

        gameManager.OnShapeToBeAcquiredSelected -= StartShapeAllocationScreen;
        gameManager.OnSlotForShapeSelected -= EndShapeAcquisitionScreen;
        gameManager.OnSlotToUpgradeSelected -= EndUpgradeScreen;
    }

    void ShowChosenShapesForLevelUp(List<SpellShapeData> chosenShapes)
    {
        for (int i = 0; i < chosenShapes.Count; i++)
        {
            buttonsForShapeAcquisition[i].shapeData = chosenShapes[i];
            if (i != chosenShapes.Count - 1)
            {
                buttonsForShapeAcquisition[i].UpdateVisualToShapeData();
            }
        }
        shapeSelectionPanel.SetActive(true);
    }

    void ShowChosenUpgradesForLevelUp(List<PlayerSlot> chosenSlots)
    {
        for (int i = 0; i < chosenSlots.Count; i++)
        {
            buttonsForUpgradeSelection[i].relatedDanceInput = chosenSlots[i].danceInput;
            buttonsForUpgradeSelection[i].button.image.sprite = chosenSlots[i].shapeIcon;
            buttonsForUpgradeSelection[i].tMPro.text = chosenSlots[i].shapeName;
        }
        upgradeSelectionPanel.SetActive(true);
    }

    void StartShapeAllocationScreen(Sprite newShapeSprite)
    {
        shapeSelectionPanel.SetActive(false);
        for (int i = 0; i < buttonsForShapeAllocation.Count; i++)
        {
            buttonsForShapeAllocation[i].button.image.sprite = shapeDatabase.playerSlots[i].shapeIcon;
            buttonsForShapeAllocation[i].shapeToBeAllocatedSprite = newShapeSprite;
            if (shapeDatabase.playerSlots[i].locked)
            {
                buttonsForShapeAllocation[i].slotLocked = true;
            }
        }
        shapeAllocationPanel.SetActive(true);
    }

    void EndShapeAcquisitionScreen()
    {
        shapeAllocationPanel.SetActive(false);
    }

    void EndUpgradeScreen()
    {
        upgradeSelectionPanel.SetActive(false);
    }
}