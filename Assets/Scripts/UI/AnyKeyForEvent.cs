using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnyKeyForEvent : MonoBehaviour
{
    public UnityEvent unityEvent;
    void Update()
    {
        if (Input.anyKeyDown && this.isActiveAndEnabled)
        {
            unityEvent.Invoke();
        }
    }

    public void PlayBackSound()
    {
        SoundManager.Instance.PlaySound(SoundType.UI_Back, true, .5f);
    }
}
