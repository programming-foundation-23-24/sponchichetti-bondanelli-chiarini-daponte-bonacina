using UnityEngine;

public abstract class BaseState : MonoBehaviour
{
    public abstract State State { get; }

    public abstract void OnStateEnter();
    public abstract void OnStateExit();
    public abstract void OnStateUpdate();
}