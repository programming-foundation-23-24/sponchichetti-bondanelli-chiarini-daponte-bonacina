using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePausedState : BaseState
{
    [SerializeField] FSM gameFSM;

    GameManager gameManager;

    public override State State => State.GamePaused;

    float previousTimeScale;

    public override void OnStateEnter()
    {
        previousTimeScale = Time.timeScale; //perch� potresti essere in Coreomanzia (slowmo)
        Time.timeScale = 0;

        gameManager = GameManager.Instance;

        if (gameManager.gamePausedByPlayer)
        {
            SoundManager.Instance.PauseGameMusic();
        }
        else if (gameManager.gameWon || !gameManager.gameStarted)
        {

        }
        else
        {
            SoundManager.Instance.LowerMusicVolume(true);
        }

    }

    public override void OnStateExit()
    {
        Time.timeScale = previousTimeScale;

        if (gameManager.gamePausedByPlayer)
        {
            SoundManager.Instance.ResumeGameMusic();
        }
        else if (gameManager.gameWon || !gameManager.gameStarted)
        {

        }
        else
        {
            SoundManager.Instance.LowerMusicVolume(false);
        }
    }

    public override void OnStateUpdate()
    {

    }
}