using System.Collections.Generic;
using UnityEngine;

public enum State
{
    GamePlaying, GamePaused
}

public class FSM : MonoBehaviour
{
    public State currentState;
    Dictionary<State, BaseState> states = new Dictionary<State, BaseState>();

    void Awake()
    {
        foreach (var state in GetComponentsInChildren<BaseState>())
        {
            states[state.State] = state;
        }
    }

    public void ChangeState(State newState)
    {
        if (newState == currentState)
        {
            return;
        }

        states[currentState].OnStateExit();
        currentState = newState;
        states[currentState].OnStateEnter();
    }

    void Update()
    {
        states[currentState].OnStateUpdate();
    }
}