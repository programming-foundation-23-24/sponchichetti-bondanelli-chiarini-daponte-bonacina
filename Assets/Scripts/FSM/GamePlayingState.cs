using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayingState : BaseState
{
    [SerializeField] FSM gameFSM;

    public override State State => State.GamePlaying;

    public override void OnStateEnter()
    {

    }

    public override void OnStateExit()
    {

    }

    public override void OnStateUpdate()
    {

    }
}