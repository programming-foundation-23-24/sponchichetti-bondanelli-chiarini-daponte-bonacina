using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;

public enum SoundType
{
    PlayerHit, EnemyEliminated, NormalCoreomancyStep, SpecialCoreomancyStep, FireElement, ThunderElement, IceElement, EarthElement, PlayerDeath, ExpPickUp, EnemyHit, LevelUp, Woosh, Boom, UI_Select, UI_Confirm, UI_Back, UI_Blocked, Victory, UI_LowBeep, UI_HighBeep, Heal, ST_cutscene, ST_titleScreen
}

public class SoundManager : Singleton<SoundManager>
{
    [Header("Audio sources")]
    [SerializeField] AudioSource effectsSourceGame;
    [SerializeField] AudioSource effectsSourceUI;
    [SerializeField] AudioSource uncompressedEffectsSource;
    [SerializeField] AudioSource musicSource1;
    [SerializeField] AudioSource musicSource2;

    [Header("Audio mixers")]
    [SerializeField] AudioMixer mixer;
    [SerializeField] AudioMixerGroup masterMixer;
    [SerializeField] AudioMixerGroup soundsMixer;
    [SerializeField] AudioMixerGroup musicMixer;
    [SerializeField] AudioMixerGroup gameSFXMixer;
    [SerializeField] AudioMixerSnapshot[] snapshots;

    [Header("Music database")]
    public AudioClip levelSoundtrackIntro;
    public AudioClip levelSoundtrackLoop1;
    public AudioClip levelSoundtrackTransition1_2;
    public AudioClip levelSoundtrackLoop2;
    public AudioClip levelSoundtrackTransition2_3;
    public AudioClip levelSoundtrackLoop3;

    [Header("Audio clips database")]
    [SerializeField] List<SoundAudioClip> soundsDatabase = new List<SoundAudioClip>();

    [Header("Mixers volume, non toccare")]
    public float masterVolume = 1;
    public float musicVolume = 1;
    public float sfxVolume = 1;


    bool alreadyElaboratingARequest;
    List<SoundType> soundTempBlackList = new List<SoundType>();

    [Header("Transitions request bools")]
    public bool soundtrackTransition1_2Requested;
    public bool soundtrackTransition2_3Requested;

    bool soundtrackTransition1_2Done, soundtrackTransition2_3Done;

    bool levelSoundtrackStarted;

    double startTime;
    double nextStartTime;

    float baseMusicVolume;

    [Header("Fade options")]
    [SerializeField] AnimationCurve fadeOutCurve;
    [SerializeField] AnimationCurve fadeInCurve;

    [SerializeField] float testBottomVolume;
    [SerializeField] float testFadeDuration;

    protected override void Awake()
    {
        base.Awake();

        baseMusicVolume = musicSource1.volume;
    }

    public void PlaySound(SoundType soundType, bool soundIsUI, float volume = 1f)
    {
        if (SoundExistsInBufferList(soundType))
        {
            return;
        }

        List<AudioClip> clipVariations = GetAudioClips(soundType);
        AudioClip clip = GetRandomVariation(clipVariations);

        PlayAudioClip(clip, volume, soundIsUI);
    }

    public void PlaySoundFromIndex(SoundType soundType, int variationIndex, bool soundIsUI, float volume = 1f)
    {
        if (SoundExistsInBufferList(soundType))
        {
            return;
        }

        List<AudioClip> clipVariations = GetAudioClips(soundType);
        if (variationIndex >= clipVariations.Count)
        {
            Debug.LogError($"SuondManager error: index requested exceeds {soundType}'s list of variations size");
            return;
        }
        AudioClip clip = GetVariationFromIndex(clipVariations, variationIndex);

        PlayAudioClip(clip, volume, soundIsUI);
    }

    public void PlayAudioClip(AudioClip clip, float volume, bool soundIsUI)
    {
        if (soundIsUI)
        {
            effectsSourceUI.PlayOneShot(clip, volume);
        }
        else
        {
            effectsSourceGame.PlayOneShot(clip, volume);
        }
    }

    public void PlaySoundDelayed(SoundType soundType, float delay, float volume = 1)
    {
        List<AudioClip> clipVariations = GetAudioClips(soundType);
        AudioClip clip = GetRandomVariation(clipVariations);
        StartCoroutine(PlayAfterDelay(clip, delay, volume));
    }

    IEnumerator PlayAfterDelay(AudioClip clip, float delay, float volume = 1)
    {
        yield return new WaitForSeconds(delay);
        uncompressedEffectsSource.PlayOneShot(clip, volume);
    }

    AudioClip GetRandomVariation(List<AudioClip> soundsVariation)
    {
        int randomIndex = Random.Range(0, soundsVariation.Count);
        return soundsVariation[randomIndex];
    }

    AudioClip GetVariationFromIndex(List<AudioClip> soundsVariation, int index)
    {
        return soundsVariation[index];
    }

    List<AudioClip> GetAudioClips(SoundType requestedSoundType)
    {
        foreach (SoundAudioClip sound in soundsDatabase)
        {
            if (sound.type == requestedSoundType)
            {
                return sound.clipVariations;
            }
        }

        Debug.LogError($"Sound {requestedSoundType} not found!");
        return null;
    }

    public void ChangeMixerVolume(string mixerNameVolume, float desiredVolume)
    {
        //avaibles mixers:
        //"masterVolume"
        //"musicVolume"
        //"sfxVolume"

        const float e = 2.7182817f;
        float sliderValue = Mathf.Pow(e, desiredVolume / 20);

        switch (mixerNameVolume)
        {
            case "masterVolume":
                masterVolume = sliderValue;
                break;
            case "musicVolume":
                musicVolume = sliderValue;
                break;
            case "sfxVolume":
                sfxVolume = sliderValue;
                break;
            default:
                Debug.LogError("A slider requested a volume change for a non existent Mixer, check the target Mixers in volume sliders");
                return;
        }

        mixer.SetFloat(mixerNameVolume, desiredVolume);
    }

    public void PlaySoundtrack(SoundType soundType, float volume)
    {
        musicSource1.loop = true;

        List<AudioClip> clipVariations = GetAudioClips(soundType);

        musicSource1.clip = clipVariations[0];
        musicSource1.volume = 0;
        musicSource1.Play();

        StartCoroutine(FadeVolume(musicSource1, volume, .5f, fadeInCurve));
    }

    public void StopSoundtrack()
    {
        StartCoroutine(StopSoundtrackAfterFadeOut());
    }

    IEnumerator StopSoundtrackAfterFadeOut()
    {
        StartCoroutine(FadeVolume(musicSource1, 0, 1f, fadeOutCurve));

        while (musicSource1.volume > 0)
        {
            yield return null;
        }

        musicSource1.Stop();

        musicSource1.loop = false;
        musicSource1.volume = baseMusicVolume;
    }

    public void StartLevelSoundtrack(double startTime)
    {
        soundtrackTransition1_2Requested = soundtrackTransition1_2Done = soundtrackTransition2_3Requested = soundtrackTransition2_3Done = false;

        musicSource1.loop = false;
        musicSource2.loop = false;
        musicSource1.volume = musicSource2.volume = baseMusicVolume;
        Debug.Log("Volume messo al valore base");
        startTime = AudioSettings.dspTime + startTime;

        musicSource1.clip = levelSoundtrackIntro;
        musicSource1.PlayScheduled(startTime);

        musicSource2.clip = levelSoundtrackLoop1;
        double introDuration = (double)musicSource1.clip.samples / musicSource1.clip.frequency;
        musicSource2.PlayScheduled(introDuration + startTime);

        double loopDuration = ((double)musicSource2.clip.samples / musicSource2.clip.frequency);
        nextStartTime = loopDuration + introDuration + startTime;

        levelSoundtrackStarted = true;
    }

    /*public IEnumerator TransitionMusic(AudioClip transitionClip, AudioClip nextLoop)
    {
        musicTransitionSource.Stop();
        musicTransitionSource.clip = transitionClip;
        musicLoopSource.loop = false;

        double currentClipDuration = (double)musicTransitionSource.clip.samples / musicTransitionSource.clip.frequency;
        double scheduledTime = currentClipDuration + startTime;
        musicTransitionSource.PlayScheduled(scheduledTime);

        yield return new WaitUntil(() => AudioSettings.dspTime > scheduledTime);

        musicLoopSource.clip = nextLoop;
        musicLoopSource.loop = true;
        musicLoopSource.PlayScheduled(AudioSettings.dspTime + musicTransitionSource.clip.length - musicTransitionSource.time);
    }*/

    /*void LoadNextSoundtrackClip(AudioClip transitionClip, AudioClip loopClip)
    {
        musicTransitionSource.clip = transitionClip;
        musicTransitionSource.PlayScheduled(nextStartTime);

        musicLoopSource.clip = loopClip;
        double introDuration = (double)musicTransitionSource.clip.samples / musicTransitionSource.clip.frequency;
        musicLoopSource.PlayScheduled(introDuration + nextStartTime);

        double loopDuration = ((double)musicLoopSource.clip.samples / musicLoopSource.clip.frequency);
        nextStartTime = loopDuration + introDuration + nextStartTime;
    }*/

    //idea soluzione: il loop non � automatico, quando sta per arrivare il nextStartTime si controlla se � stata richiesta la fase successiva, se non lo � stata di rimette lo stesso pezzo
    //ci� vuol dire fare un loader generico (non insieme intro e loop) e non distinguere le due audiosource https://johnleonardfrench.com/ultimate-guide-to-playscheduled-in-unity/#play_scheduled_intro

    void LoadNextSoundtrackClip(AudioClip nextClip)
    {
        AudioSource freeAudioSource;

        if (musicSource1.isPlaying)
        {
            freeAudioSource = musicSource2;
        }
        else
        {
            freeAudioSource = musicSource1;
        }

        freeAudioSource.clip = nextClip;
        freeAudioSource.PlayScheduled(nextStartTime);

        double clipDuration = (double)freeAudioSource.clip.samples / freeAudioSource.clip.frequency;

        nextStartTime = clipDuration + nextStartTime;
    }

    public void StopLevelMusic()
    {
        float fadeOutTime = SceneTransitionController.Instance.TransitionTime;

        StartCoroutine(FadeVolume(musicSource1, 0, fadeOutTime, fadeOutCurve));
        StartCoroutine(FadeVolume(musicSource2, 0, fadeOutTime, fadeOutCurve));

        musicSource1.SetScheduledEndTime(AudioSettings.dspTime + fadeOutTime + 0.001f);
        musicSource2.SetScheduledEndTime(AudioSettings.dspTime + fadeOutTime + 0.001f);

        levelSoundtrackStarted = false;
        soundtrackTransition1_2Requested = soundtrackTransition1_2Done = soundtrackTransition2_3Requested = soundtrackTransition2_3Done = false;
    }

    private void Update()
    {
        if (!levelSoundtrackStarted)
        {
            return;
        }

        if (AudioSettings.dspTime > nextStartTime - 1  && GameManager.Instance != null)
        {
            if (!soundtrackTransition1_2Requested)
            {
                LoadNextSoundtrackClip(levelSoundtrackLoop1);
            }
           else if (soundtrackTransition1_2Requested && !soundtrackTransition1_2Done)
            {
                LoadNextSoundtrackClip(levelSoundtrackTransition1_2);
                soundtrackTransition1_2Done = true;
            }
            else if (soundtrackTransition1_2Done && !soundtrackTransition2_3Requested)
            {
                LoadNextSoundtrackClip(levelSoundtrackLoop2);
            }
            else if (soundtrackTransition2_3Requested && !soundtrackTransition2_3Done)
            {
                LoadNextSoundtrackClip(levelSoundtrackTransition2_3);
                soundtrackTransition2_3Done = true;
            }
            else if (soundtrackTransition2_3Done)
            {
                LoadNextSoundtrackClip(levelSoundtrackLoop3);
            }
        }
    }

    public void PauseGameMusic()
    {
        //StartCoroutine(ChangeMixerSmoothly(pauseMusicMixer));
        float[] weights = new float[2];
        weights[0] = 0; weights[1] = 1;
        mixer.TransitionToSnapshots(snapshots, weights, .2f);
    }
    public void ResumeGameMusic()
    {
        //StartCoroutine(ChangeMixerSmoothly(null));

        float[] weights = new float[2];
        weights[0] = 1; weights[1] = 0;
        mixer.TransitionToSnapshots(snapshots, weights, .2f);
    }

    public void LowerMusicVolume(bool lower)
    {
        if (lower)
        {
            StartCoroutine(FadeVolume(musicSource1, baseMusicVolume * 2 / 3, .2f, fadeOutCurve));
            StartCoroutine(FadeVolume(musicSource2, baseMusicVolume * 2 / 3, .2f, fadeOutCurve));
        }
        else
        {
            StartCoroutine(FadeVolume(musicSource1, baseMusicVolume, .2f, fadeInCurve));
            StartCoroutine(FadeVolume(musicSource2, baseMusicVolume, .2f, fadeInCurve));
        }

        float[] weights = new float[2];

        if (lower)
        {
            weights[0] = .6f; weights[1] = .4f;
        }
        else
        {
            weights[0] = 1; weights[1] = 0;
        }

        mixer.TransitionToSnapshots(snapshots, weights, .2f);
    }
    /*
    IEnumerator ChangeMixerSmoothly(AudioMixerGroup mixerGroup)
    {
        float lowestTransitionVolume = testBottomVolume;
        float fadeDuration = testFadeDuration;

        StartCoroutine(FadeVolume(musicSource1, lowestTransitionVolume, fadeDuration, fadeOutCurve));
        StartCoroutine(FadeVolume(musicSource2, lowestTransitionVolume, fadeDuration, fadeOutCurve));

        yield return new WaitForSecondsRealtime(fadeDuration);

        musicSource1.outputAudioMixerGroup = mixerGroup;
        musicSource2.outputAudioMixerGroup = mixerGroup;

        yield return new WaitForSecondsRealtime(.05f);

        StartCoroutine(FadeVolume(musicSource1, baseMusicVolume, fadeDuration/2, fadeInCurve));
        StartCoroutine(FadeVolume(musicSource2, baseMusicVolume, fadeDuration/2, fadeInCurve));
    }
    */

    IEnumerator FadeVolume(AudioSource audioSource, float desiredVolume, float fadeTime, AnimationCurve fadeCurve)
    {
        float elapsedTime = 0;

        float startingVolume = audioSource.volume;

        while (elapsedTime < fadeTime)
        {
            float normalizedElapsedTime = Mathf.InverseLerp(0.0f, fadeTime, elapsedTime);
            float evaluatedTimeByCurve = fadeCurve.Evaluate(normalizedElapsedTime);

            audioSource.volume = Mathf.Lerp(startingVolume, desiredVolume, evaluatedTimeByCurve);
            elapsedTime += Time.unscaledDeltaTime;

            yield return null;
        }

        audioSource.volume = desiredVolume;

        Debug.Log($"Volume fadeato a {desiredVolume}");
    }

    IEnumerator AddToTempBlacklist(SoundType soundType)
    {
        soundTempBlackList.Add(soundType);
        yield return new WaitForSecondsRealtime(.005f);
        soundTempBlackList.Remove(soundType);
    }

    bool SoundExistsInBufferList(SoundType soundType)
    {
        foreach (SoundType sound in soundTempBlackList)
        {
            if (sound == soundType)
            {
                Debug.LogWarning($"{sound} sound has been blocked");
                return true;
            }
        }

        StartCoroutine(AddToTempBlacklist(soundType));

        return false;
    }
}