using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum DanceInput
{
    Up, Left, Down, Right, ElementInput, ShapeInput
}

public enum Element
{
    None, Fire, Thunder, Ice, Earth
}

/* possibile ottimizzazione elementi
public class FireElement : BaseElement
{
    public override Element Element => Element.Fire;
}

public abstract class BaseElement : MonoBehaviour
{
    public abstract Element Element { get; }
}*/

public class GameManager : Singleton<GameManager>
{
    [Header("Game area size")]
    public Vector2 gameAreaSize = new Vector2();

    [Header("Player magnetism range")]
    public float expMagnetismRange;

    [Header("Element Graphics")]
    public List<Color> elementsColors = new List<Color>();

    [Header("Slowmo Coreomanzia")]
    [Range(0.01f, 1.0f)] public float TimeScaleDuringCoreomancy;

    [Header("QOL toggles")]
    public bool firstShapeThenElementInCoreomancy;
    public bool showDamageNumber;

    public bool gamePausedByPlayer;

    [Header("UI (da collegare)")]
    public HealthUI healthUI;
    public ExperienceBarUI experienceBarUI;
    public CoreomancyUI coreomancyUI;

    [Header("Men�/UI (sono GO) (da collegare)")]
    public GameObject inGameUI;
    public GameObject gameOverMenu;
    public GameObject pauseMenu;
    public GameObject winMenu;
    public GameObject winVFX;
    [SerializeField] GameObject introCountUp;

    [Header("Durata Partita in secondi")]
    public float GameTime = 600f;
    public float currentGameTime = 0f;

    public float introCountAnimationLenght = .5f;

    SpellShapeData selectedShapeDuringUpgrade;

    [Header("Logics (non da collegare)")]
    public PlayerHealth playerHealth;
    public PlayerController playerController;
    public PlayerSpellShapesDatabase playerSpellShapesDatabase;
    PlayerSpellShapesDatabase playerSpellInventory;
    SpellShooter playerSpellShooter;
    Transform playerBodyTransform;
    GameObject enemySpawnerManager;
    public FSM gameFSM;
    List<MineProjectile> spawnedMines = new List<MineProjectile>();
    public bool gameWon = false;
    public bool gameStarted = false;
    bool howToPlayScreenActive = false; //mamma se puzza ma non c'� tempo

    public ExpManager expManager;

    public DamageAndElementsLogic damageLogic;

    public CursorManager cursorManager;

    //LEVEL UP EVENTS, collegati all'ui
    public event Action<Sprite> OnShapeToBeAcquiredSelected;
    public event Action OnSlotForShapeSelected;
    public event Action OnSlotToUpgradeSelected;
    public event Action OnLevelUpDone;

    //EVENTS
    public event Action OnGameWon;

    //public event Action<BaseEnemy, BaseProjectile> OnEnemyHitBySpell;

    //public event Action OnThunderSpellGenerated;
    //public event Action<BaseEnemy> OnFireCastedByEnemyHit;

    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(Vector3.zero, (Vector3)gameAreaSize);
    }*/

    protected override void Awake()
    {
        base.Awake();

        ReferencesSetUp();

        UpdateExpUI(0, expManager.CalculateExpToLevelUp(), expManager.currentLevel, expManager.maxLevel);

        ResetGameTime();

        CheckCoreomancyOrder();

        cursorManager.SetCrossHairAsCursor();
    }

    void ReferencesSetUp()
    {
        PlayerController eligiblePlayerController = FindAnyObjectByType<PlayerController>();
        PlayerSpellShapesDatabase eligiblePlayerSpellShapeDatabase = FindAnyObjectByType<PlayerSpellShapesDatabase>();

        if (eligiblePlayerController != null)
        {
            playerController = eligiblePlayerController;
        }
        else
        {
            Debug.LogError("PLAYER CONTROLLER NON TROVATO DAL GAME MANAGER!");
        }

        if (eligiblePlayerSpellShapeDatabase != null)
        {
            playerSpellShapesDatabase = eligiblePlayerSpellShapeDatabase;
        }
        else
        {
            Debug.LogError("PLAYER CONTROLLER NON TROVATO DAL GAME MANAGER!");
        }

        if (!playerController.gameObject.TryGetComponent(out playerHealth))
        {
            Debug.LogError("PLAYER HEALTH NON TROVATO DAL GAME MANAGER!");
        }

        playerSpellShooter = playerController.gameObject.GetComponentInChildren<SpellShooter>();

        if (playerSpellShooter == null)
        {
            Debug.LogError("SPELL SHOOTER NON TROVATO DAL GAME MANAGER!");
        }

        playerBodyTransform = playerController.bodyTransform;

        enemySpawnerManager = FindAnyObjectByType<EnemySpawner>().gameObject;
        enemySpawnerManager.SetActive(false);

        if (!TryGetComponent(out gameFSM))
        {
            Debug.LogError("Game FSM NON TROVATA DAL GAME MANAGER!");
        }
    }

    void CheckCoreomancyOrder()
    {
        if (firstShapeThenElementInCoreomancy)
        {
            playerController.firstInputInCoreomancy = DanceInput.ShapeInput;
            playerController.lastInputInCoreomancy = DanceInput.ElementInput;
        }
        else
        {
            playerController.firstInputInCoreomancy = DanceInput.ElementInput;
            playerController.lastInputInCoreomancy = DanceInput.ShapeInput;
        }
    }

    void Start()
    {
        StartCoroutine(CountUpBeforeStart());
    }

    IEnumerator CountUpBeforeStart()
    {
        PausePlayTime();
        introCountUp.SetActive(false);

        yield return new WaitForSecondsRealtime(SceneTransitionController.Instance.TransitionTime);

        introCountUp.SetActive(true);

        SoundManager.Instance.StartLevelSoundtrack(introCountAnimationLenght);

        /*
        yield return new WaitForSecondsRealtime(soundtrackBeatLenght * 3);

        SoundManager.Instance.StartLevelSoundtrack();

        yield return new WaitForSecondsRealtime(soundtrackBeatLenght * 2);

        gameStarted = true;
        */

        yield return new WaitUntil(() => gameStarted);

        ResumePlayTime();
        enemySpawnerManager.SetActive(true);

        yield return new WaitForSecondsRealtime(1f);

        introCountUp.SetActive(false);
    }

    public void StartGame()
    {
        gameStarted = true;
    }

    public void Update()
    {
        currentGameTime += Time.deltaTime;
        CheckTimesOver();

        if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            if (gameFSM.currentState == State.GamePlaying)
            {
                PauseGame();
            }
            else if (gameFSM.currentState == State.GamePaused && gamePausedByPlayer && !howToPlayScreenActive)
            {
                ResumeGame();
            }
        }


        if (itsTimeForSoundtrackTransitionTo3rdLoop() && !SoundManager.Instance.soundtrackTransition2_3Requested)
        {
            SoundManager.Instance.soundtrackTransition2_3Requested = true;
        }
        else if (itsTimeForSoundtrackTransitionTo2ndLoop() && !SoundManager.Instance.soundtrackTransition1_2Requested)
        {
            SoundManager.Instance.soundtrackTransition1_2Requested = true;
        }


    }

    bool itsTimeForSoundtrackTransitionTo3rdLoop()
    {
        if (currentGameTime >= 380 && currentGameTime < 600)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool itsTimeForSoundtrackTransitionTo2ndLoop()
    {
        if (currentGameTime >= 160 && currentGameTime < 380)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PausePlayTime()
    {
        gameFSM.ChangeState(State.GamePaused);
    }

    public void ResumePlayTime()
    {
        gameFSM.ChangeState(State.GamePlaying);
    }

    public void ResetGameTime()
    {
        gameFSM.ChangeState(State.GamePlaying);
        Time.timeScale = 1;
    }

    void PauseGame()
    {
        cursorManager.SetDefaultPointerAsCursor();
        gamePausedByPlayer = true;
        PausePlayTime();
        pauseMenu.SetActive(true);
        inGameUI.SetActive(false);
    }

    void ResumeGame()
    {
        cursorManager.SetCrossHairAsCursor();
        inGameUI.SetActive(true);
        pauseMenu.SetActive(false);
        ResumePlayTime();
        gamePausedByPlayer = false;
    }

    public void StartCoreomancySlowMotion()
    {
        Time.timeScale = TimeScaleDuringCoreomancy;
    }

    public void StopCoreomancySlowMotion()
    {
        Time.timeScale = 1;
    }

    public void CheckTimesOver()
    {
        if (currentGameTime >= GameTime && !gameWon)
        {
            GameOver(true);
        }
    }

    public void GameOver(bool victory)
    {
        if (!victory) //Se perdi aka muori
        {
            SoundManager.Instance.PlaySoundDelayed(SoundType.PlayerDeath, .5f, .3f);
            StartCoroutine(WaitBeforeGameOverMenu());
        }
        else // Se Vinci
        {
            gameWon = true;
            enemySpawnerManager.SetActive(false);
            OnGameWon.Invoke();
            SoundManager.Instance.PlaySoundDelayed(SoundType.Victory, .5f, .6f);
            WinVFX();
            StartCoroutine(WaitBeforeWinningMenu());
        }

        SoundManager.Instance.StopLevelMusic();
    }

    void WinVFX()
    {
        if (winVFX != null)
        {
            winVFX.SetActive(true);
        }
    }

    IEnumerator WaitBeforeGameOverMenu()
    {
        yield return new WaitForSeconds(3);
        cursorManager.SetDefaultPointerAsCursor();
        PausePlayTime();
        gameOverMenu.SetActive(true);
    }
    IEnumerator WaitBeforeWinningMenu()
    {
        yield return new WaitForSeconds(3);
        cursorManager.SetDefaultPointerAsCursor();
        PausePlayTime();
        winMenu.SetActive(true);
    }

    public void ActivateCoreomancy()
    {
        StartCoreomancySlowMotion();
        coreomancyUI.TurnOnSlowMoVFX();
    }

    public void DeactivateCoreomancy()
    {
        StopCoreomancySlowMotion();
        coreomancyUI.TurnOffSlowMoVFX();
    }

    public void EnemyHitByPlayerProjectile(BaseEnemy enemy, BaseProjectile projectile)
    {
        damageLogic.DealProjectileDamageAndElementToEnemy(enemy, projectile);

        projectile.TryGetComponent(out ElementalProperty elementalProperty);

        if (elementalProperty.currentElement == Element.Earth || elementalProperty.currentElement == Element.Ice || projectile.motionValueByLevel[projectile.projectileLevel] <= 0)
        {
            return;
        }

        enemy.StartCoroutine(enemy.Knockback(projectile.transform.position, enemy.defaultKnockbackDistance, enemy.deafultKnockbackDuration));
    }

    public void PlayerGotHit(Vector3 hitterPosition, bool knockback)
    {
        if (!playerHealth.tookDamageRecently)
        {
            playerController.CoreomancyFailed();

            if (knockback)
            {
                playerController.StartCoroutine(playerController.Knockback(hitterPosition));
            }

            damageLogic.DealDamageToPlayer(1, playerHealth);

            SoundManager.Instance.PlaySound(SoundType.PlayerHit, false, .9f);
        }
    }

    public void ShowDamageNumber(BaseEnemy enemy, int damage)
    {
        if (showDamageNumber)
        {
            DamageNumber dmg = Instantiate(damageLogic.damageNumberPrefab, enemy.transform.position + new Vector3(0, .5f, -.1f), Quaternion.identity);
            dmg.ShowMe(damage);
        }
    }

    public void SelectedShapeToAcquire(SpellShapeData shapeData)
    {
        OnShapeToBeAcquiredSelected?.Invoke(shapeData.icon);
        selectedShapeDuringUpgrade = shapeData;
    }

    public void SelectedSlotForShape(DanceInput input)
    {
        OnSlotForShapeSelected?.Invoke();
        PlayerSpellShapesDatabase.Instance.AcquireSpecificShape(input, selectedShapeDuringUpgrade);
    }

    public void SelectedSlotToUpgrade(DanceInput input)
    {
        OnSlotToUpgradeSelected?.Invoke();
        PlayerSpellShapesDatabase.Instance.UpgradeSpecificSlot(input);
    }

    public Transform GetPlayerTransform()
    {
        return playerController.transform;
    }

    public Vector2 GetPlayerPosition()
    {
        return playerController.transform.position;
    }

    public Vector3 GetPlayerSpellShooterPosition()
    {
        return playerSpellShooter.transform.position;
    }

    public Transform GetPlayerBodyTransform()
    {
        return playerBodyTransform;
    }

    public Element GetPlayerActiveElement()
    {
        return playerController.activeElement;
    }

    public void AddToPlayerSpeed(float addendum)
    {
        playerController.movementSpeed += addendum;
    }

    public int GetPlayerCurrentHealth()
    {
        return playerHealth.currentHealth;
    }

    public int GetPlayerMaxHealth()
    {
        return playerHealth.maxHealth;
    }

    public bool CheckIfOutOfBounds(Vector2 position)
    {
        return Mathf.Abs(position.x) >= gameAreaSize.x || Mathf.Abs(position.y) >= gameAreaSize.y;
    }

    public void UpdateHealthUI()
    {
        healthUI.HearthsUpdater();
    }

    public void PickUpGem(int gemValue)
    {
        expManager.AddExp(gemValue);
    }

    public void UpdateExpUI(float actualExp, float ExpForNextLevel, int currentLevel, int maxLevel)
    {
        experienceBarUI.UpdateExpBar(actualExp, ExpForNextLevel, currentLevel, maxLevel);
    }

    public void EndLevelUp()
    {
        OnLevelUpDone?.Invoke();
        expManager.levelUpRequestCompleted = true;
    }
    
    public void AddMineToList(MineProjectile mine, int maxNumber)
    {
        spawnedMines.Add(mine);
        if (spawnedMines.Count > maxNumber)
        {
            Destroy(spawnedMines[0].gameObject);
            spawnedMines.RemoveAt(0);
        }
    }

    public void RemoveMineFromList(MineProjectile mine)
    {
        spawnedMines.Remove(mine);
    }

    public void InvertCoreomancyStepsOrder()
    {
        firstShapeThenElementInCoreomancy = !firstShapeThenElementInCoreomancy;
        DanceInput tempInput = playerController.firstInputInCoreomancy;
        playerController.firstInputInCoreomancy = playerController.lastInputInCoreomancy;
        playerController.lastInputInCoreomancy = tempInput;
    }

    public void ToggleDamageNumbers()
    {
        showDamageNumber = !showDamageNumber;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(transform.position ,2 * gameAreaSize);
    }

    public void HowToPlayScreenOn()
    {
        howToPlayScreenActive = true;
    }

    public void HowToPlayScreenOff()
    {
        howToPlayScreenActive = false;
    }
}