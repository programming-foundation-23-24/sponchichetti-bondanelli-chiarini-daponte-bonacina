using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public abstract class BaseContactChecker : MonoBehaviour
{
    protected abstract void OnTriggerContactEnter(Collider2D otherCollision);
    protected abstract void OnCollisionContactEnter(Collision2D otherCollision);
    
    private void OnTriggerEnter2D(Collider2D otherCollision)
    {
        OnTriggerContactEnter(otherCollision);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnCollisionContactEnter(collision);
    }
}