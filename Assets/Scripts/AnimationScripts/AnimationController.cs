using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public abstract class AnimationController : MonoBehaviour
{
    public Animator animator;

    public virtual void Update()
    {

    }
}
