using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionActorAnimController : AnimationController
{
    public Transform targetPosition;
    bool isMoving = true;
    public float moveSpeed;
    

    public override void Update()
    {
        if (isMoving)
        {
            Vector3 newPosition = Vector3.MoveTowards(transform.position, targetPosition.position, moveSpeed * Time.deltaTime);
            transform.position = newPosition;

            if (transform.position == targetPosition.position)
            {
                isMoving = false;

                    animator.SetTrigger("Idle");
                
            }
        }
    }
}

