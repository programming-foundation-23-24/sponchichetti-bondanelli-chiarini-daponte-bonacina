﻿using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    //Condivide molto con baseprojectile, se si dovessero separare spell e projectiles accorpare questo script
    
    public float travelSpeed;
    public float range;
    float distanceTravelled;
    Vector2 originPoint;
    Vector2 targetPoint;
    Vector2 travelDirection;

    void Awake()
    {
        targetPoint = GameManager.Instance.GetPlayerPosition();
        originPoint = transform.position;

        travelDirection = (targetPoint - originPoint).normalized;
        float angle = Mathf.Atan2(travelDirection.y, travelDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    void Update()
    {
        Vector3 travelDistanceVector = Time.deltaTime * travelSpeed * (Vector3)travelDirection;
        transform.position += travelDistanceVector;

        distanceTravelled += travelDistanceVector.magnitude;
        if (distanceTravelled >= range || GameManager.Instance.gameWon == true)
        {
            Destroy(gameObject);
        }
    }
}
