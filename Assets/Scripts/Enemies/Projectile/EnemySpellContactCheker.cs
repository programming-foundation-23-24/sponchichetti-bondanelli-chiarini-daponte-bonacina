﻿using UnityEngine;

public class EnemySpellContactCheker : BaseContactChecker
{
    protected override void OnCollisionContactEnter(Collision2D otherCollision)
    {
    }

    protected override void OnTriggerContactEnter(Collider2D otherCollision)
    {
        if (otherCollision.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}