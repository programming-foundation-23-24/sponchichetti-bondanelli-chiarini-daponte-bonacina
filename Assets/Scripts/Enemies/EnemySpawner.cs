using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;

public class EnemySpawner : MonoBehaviour
{
    public EnemyInfo enemy;

    [SerializeField] Transform playerTransform; //serialized per il gizmos

    //[SerializeField] float spawnCircleRadius = 10f;

    [SerializeField] float spawnAreaSizeOffset = 2f;

    //public List<float> spawnTimerByMinute = new List<float>();
    public List<int> spawnQuantityByMinute = new List<int>();

    [Header("Non toccare")]
    [SerializeField] int currentMinute;
    [SerializeField] float spawnIntervalThisMinute = 1;
    [SerializeField] float frequencyTimer = 0;
    [SerializeField] float currentSecondsTimer = 0;

    public int spawnCount = 0;

    private void Start()
    {
        //playerTransform = GameManager.Instance.GetPlayerTransform();
        spawnIntervalThisMinute = CalculateSpawnInterval();
        frequencyTimer = spawnIntervalThisMinute;
    }

    private void Update()
    {
        //Debug.Log((int)Time.timeSinceLevelLoad / 60);

        frequencyTimer -= Time.deltaTime;
        if (frequencyTimer <= 0)
        {
            SpawnEnemy();
            frequencyTimer = spawnIntervalThisMinute;
        }

        currentSecondsTimer += Time.deltaTime;
        if (currentSecondsTimer >= 60f)
        {
            SpawnRemainingEnemies(currentMinute);
            spawnIntervalThisMinute = CalculateSpawnInterval();
            frequencyTimer = spawnIntervalThisMinute;
        }
        currentSecondsTimer = GameManager.Instance.currentGameTime % 60;

        
        if (Mathf.Abs((int)(GameManager.Instance.currentGameTime) - (currentMinute * 60 + currentSecondsTimer)) > 30)
        {
            spawnIntervalThisMinute = CalculateSpawnInterval();
            frequencyTimer = spawnIntervalThisMinute;
        }
        
    }

    float CalculateSpawnInterval()
    {
        currentMinute = (int)GameManager.Instance.currentGameTime / 60;
        currentSecondsTimer = (int)GameManager.Instance.currentGameTime % 60;
        if (currentMinute >= spawnQuantityByMinute.Count)
        {
            return 100;     //numero qualsiasi maggiore di 60, ergo non spawna nulla
        }

        if (spawnQuantityByMinute[currentMinute] > 0)
        {
            float spawnInterval = 60f / spawnQuantityByMinute[currentMinute];
            return spawnInterval;
        }
        else
        {
            return 100;     //numero qualsiasi maggiore di 60, ergo non spawna nulla
        }

    }

    void SpawnEnemy()
    {
        Instantiate(enemy.enemyPrefab, CaluclateSpawnPointOnRectanglePerimeter(), Quaternion.identity);
        spawnQuantityByMinute[currentMinute]--;
        spawnCount++;
        Debug.Log($"Enemy Spawned: {enemy.enemyPrefab.name}");
    }

    void SpawnRemainingEnemies(int minute)
    {
        while (spawnQuantityByMinute[minute] > 0)
        {
            SpawnEnemy();
        }
    }
    /*
    Vector3 CalculateSpawnPointOnCircumference()
    {
        //Calcola punto randomico su una circonferenza
        float randomAngle = Random.Range(0f, 2f * Mathf.PI);
        float x = Mathf.Cos(randomAngle) * spawnCircleRadius;
        float y = Mathf.Sin(randomAngle) * spawnCircleRadius;
        Vector2 randomPointOnCircle = new Vector2(x, y);

        Vector3 SpawnPosition = (Vector3)randomPointOnCircle + playerTransform.position;
        return SpawnPosition;
    }
    */
    Vector3 CaluclateSpawnPointOnRectanglePerimeter()
    {
        float cameraVerticalSize = Camera.main.orthographicSize * 2;
        float cameraHorizontalSize = cameraVerticalSize * (1920f / 1080f);

        float spawnAreaVerticalSize = cameraVerticalSize + spawnAreaSizeOffset;
        float spawnAreaHorizontalSize = cameraHorizontalSize + spawnAreaSizeOffset;

        float spawnAreaPerimeterSize = 2 * spawnAreaHorizontalSize + 2 * spawnAreaVerticalSize;

        float randomPointOnFlattenedPerimeter = Random.Range(0f, spawnAreaPerimeterSize);

        float randomX, randomY;

        if (randomPointOnFlattenedPerimeter < spawnAreaHorizontalSize) //primo lato in alto
        {
            randomX = randomPointOnFlattenedPerimeter - spawnAreaHorizontalSize / 2;
            randomY = spawnAreaVerticalSize / 2;
        }
        else if (randomPointOnFlattenedPerimeter < spawnAreaHorizontalSize + spawnAreaVerticalSize) //secondo lato a destra
        {
            randomX = spawnAreaHorizontalSize / 2;
            randomY = (randomPointOnFlattenedPerimeter - spawnAreaHorizontalSize) - spawnAreaVerticalSize / 2;
        }
        else if (randomPointOnFlattenedPerimeter < 2 * spawnAreaHorizontalSize + spawnAreaVerticalSize) //terzo lato in basso
        {
            randomX = (randomPointOnFlattenedPerimeter - (spawnAreaHorizontalSize + spawnAreaVerticalSize)) - spawnAreaHorizontalSize / 2;
            randomY = - (spawnAreaVerticalSize / 2);
        }
        else //quarto lato a sinistra
        {
            randomX = - (spawnAreaHorizontalSize / 2);
            randomY = (randomPointOnFlattenedPerimeter - (2 * spawnAreaHorizontalSize + spawnAreaVerticalSize)) - spawnAreaVerticalSize / 2;
        }

        Vector3 RandomPositionOnBaseSpawnPerimeter = new Vector3(randomX, randomY);

        return RandomPositionOnBaseSpawnPerimeter + playerTransform.position;
    }
    
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(playerTransform.position, spawnCircleRadius);

        float cameraVerticalSize = Camera.main.orthographicSize * 2;
        float cameraHorizontalSize = cameraVerticalSize * (1920f/1080f);
        Gizmos.DrawWireCube(playerTransform.position, new Vector3 (cameraHorizontalSize + spawnAreaSizeOffset, cameraVerticalSize + spawnAreaSizeOffset));
    }
}