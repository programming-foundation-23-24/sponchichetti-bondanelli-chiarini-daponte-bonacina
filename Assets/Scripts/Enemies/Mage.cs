using System.Collections;
using UnityEngine;

public class Mage : BaseEnemy
{
    [SerializeField] GameObject projectilePrefab;

    public float attackChargeTime;
    //public float attackTime;
    public float attackCooldown;
    public float attackDamage;
    bool isAttacking;
    bool isCasting;
    bool isRecovering;
    float timer;

    protected override void Update()
    {
        if (isCasting)
        {
            if (isFreezed)
            {
                EndAttack();
                return;
            }

            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                CastAttack();
            }
            return;
        }
        else if (isRecovering)
        {
            if (isFreezed)
            {
                return;
            }

            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                EndAttack();
            }
            return;
        }

        base.Update();
    }


    protected override void Attack()
    {
        //Debug.Log("ATTACCO");
        if (CanAttack())
        {
            StartCasting();
        }
    }

    void StartCasting()
    {
        animator.SetTrigger("Attack");
        isAttacking = true;
        isCasting = true;
        timer = attackChargeTime;
    }

    void CastAttack()
    {
        isCasting = false;
        Instantiate(projectilePrefab, transform.position, Quaternion.identity);
        StartRecovering();
        
    }

    void StartRecovering()
    {
        isRecovering = true;
        timer = attackCooldown;
    }

    void EndAttack()
    {
        animator.SetTrigger("Move");
        isCasting = false;
        isRecovering = false;
        isAttacking = false;
    }

    bool CanAttack()
    {
        if (isAttacking)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /*IEnumerator ShootMagic()
    {
    isAttacking = true;
    isCasting = true;
    yield return new WaitForSeconds(attackChargeTime);
    Instantiate(projectilePrefab, transform.position, Quaternion.identity);
    isCasting = false;
    yield return new WaitForSeconds(attackTime);
    yield return new WaitForSeconds(attackCooldown);
    isAttacking = false;
    }*/
}