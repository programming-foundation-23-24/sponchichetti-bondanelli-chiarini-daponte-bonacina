using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidOverlappingColliders : MonoBehaviour
{
    [SerializeField] Transform parentOfBody;
    Collider2D enemyCollider;
    float pushDistance;
    float myWeight;

    private void Awake()
    {
        Collider2D eligibleCollider = GetComponent<BoxCollider2D>();
        if (eligibleCollider == null)
        {
            Debug.LogError($"{this} missing collider2D");
        }
        else
        {
            enemyCollider = eligibleCollider;
            pushDistance = enemyCollider.bounds.size.magnitude;
        }

        BaseEnemy thisEnemy;
        if (!gameObject.TryGetComponent(out thisEnemy))
        {
            Debug.LogError($"{this} missing baseEnemy script");
        }

        myWeight = thisEnemy.weight;
    }

    private void FixedUpdate()
    {
        List<Collider2D> otherColliders = new List<Collider2D>();
        ContactFilter2D contactFilter = new ContactFilter2D();
        contactFilter.NoFilter();
        Physics2D.OverlapBox(transform.position, enemyCollider.bounds.size * 1.25f, 1, contactFilter, otherColliders);
        //overlap prende anche s� stesso, come per baseEnemy si potrebbe risolvere con un collider.cast o collider.raycast
            //non si pu�: nessuno dei due funzionerebbe (il primo non funziona senza rigidbody ed il secondo non usa il concetto di area)
        
        if (otherColliders.Count > 0)
        {
            BaseEnemy heavierEnemyColliding = null;
            float heavierEnemysWeight = 0f;
            int heavierIndex = 0;

            for (int i = 0; i < otherColliders.Count; i++)
            {
                if (otherColliders[i] != enemyCollider)
                {
                    BaseEnemy otherEnemy;
                    if (otherColliders[i].TryGetComponent(out otherEnemy))
                    {
                        if (otherEnemy.weight > heavierEnemysWeight)
                        {
                            heavierEnemysWeight = otherEnemy.weight;
                            heavierEnemyColliding = otherEnemy;
                            heavierIndex = i;
                        }
                    }
                }
            }

            if (heavierEnemysWeight < myWeight)
            {
                return;
            }

            Vector2 pushDirection = -(heavierEnemyColliding.transform.position - transform.position).normalized;

            if (pushDirection == Vector2.zero)
            {
                if (Random.value > .5f)
                {
                    transform.position += Vector3.up * 0.03f;
                }
                else
                {
                    transform.position += Vector3.right * 0.03f;
                }
                return;
            }

            pushDistance = otherColliders[heavierIndex].bounds.size.magnitude;

            if (pushDistance > 1f)
            {
                pushDistance = 1f;
            }
            else if (pushDistance < .1f)
            {
                pushDistance = .1f;
            }

            float scaledPushDistance = pushDistance * Time.fixedDeltaTime;

            RaycastHit2D colliderHitOnDirection = Physics2D.BoxCast(transform.position, enemyCollider.bounds.size * .9f, 0f, pushDirection, scaledPushDistance);

            if (!colliderHitOnDirection || colliderHitOnDirection.collider == otherColliders[heavierIndex] || colliderHitOnDirection.collider == enemyCollider || (colliderHitOnDirection.collider.GetComponent<BaseEnemy>().weight <= myWeight && colliderHitOnDirection.collider != enemyCollider))
            {
                transform.position += (Vector3)pushDirection * scaledPushDistance;
            }

            otherColliders.Clear();
        }
    }
}