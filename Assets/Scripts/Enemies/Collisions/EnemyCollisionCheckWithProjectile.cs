﻿using UnityEngine;


public abstract class BaseProj : MonoBehaviour
{
    public float damage;
}

public class EnemyProj : BaseProj
{

}


public abstract class PlayerProj : BaseProj
{
    public Element element;
}

public class ConeProj : PlayerProj
{

}




/*public class EnemyCollisionCheckWithProjectile : MonoBehaviour
{
    public BaseEnemy enemy;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerProjectile"))
        {

            PlayerProj proj = collision.gameObject.GetComponent<PlayerProj>();

            if (proj != null)
            {
                enemy.TakeDamage(proj.damage);

                GameManager.Instance.EnemyHasBeenHitByPlayerProjectile(new Data() { element = proj.element });
            }
        }
    }
}*/