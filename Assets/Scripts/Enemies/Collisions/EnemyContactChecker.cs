using System;
using UnityEngine;

public class EnemyContactChecker : BaseContactChecker
{
    BaseEnemy enemy;

    void Start()
    {
        TryGetComponent(out enemy);
    }

    protected override void OnTriggerContactEnter(Collider2D otherCollision)
    {
        if (otherCollision.TryGetComponent(out BaseProjectile projectile))
        {
            if (enemy.invincible)
            {
                return;
            }

            foreach (BaseEnemy otherEnemy in projectile.alreadyHitEnemies)
            {
                if (otherEnemy == enemy)
                {
                    return;
                }
            }

            GameManager.Instance.EnemyHitByPlayerProjectile(enemy, projectile);
            projectile.StartCoroutine(projectile.AddEnemyToHitList(enemy));
        }
    }

    protected override void OnCollisionContactEnter(Collision2D otherCollision)
    {
    }
}