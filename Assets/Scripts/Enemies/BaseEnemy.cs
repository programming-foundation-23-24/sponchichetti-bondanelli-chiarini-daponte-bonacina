using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.IO.LowLevel.Unsafe;
using Unity.VisualScripting;
using UnityEngine;


public abstract class BaseEnemy : MonoBehaviour
{
    [Header("GENERAL STATS")]
    public float healthPoints;
    public float movementSpeed;
    float actualMovementSpeed;
    public float attackRange;
    public float weight;
    public int dropQuantity;

    [Header("(Da collegare)")]
    public ExpGemSpawner gemSpawner;
    public List<GameObject> OnDeathParticlesToInstantiate;
    [SerializeField] SpriteRenderer spriteRenderer;

    [Header("STATUS")]
    public bool isBurning;
    public float burningTimer = 0;
    public bool isFreezed;
    public float freezedTimer = 0;
    public Coroutine elementalStatusCoroutine;

    [SerializeField] float spawnInvincibilityDuration;
    public bool invincible;

    public float defaultKnockbackDistance;
    public float deafultKnockbackDuration; //deve essere minore della durata del damage buffer
    public AnimationCurve knockbackCurve = AnimationCurve.Linear(0, 0, 1, 1);
    bool immobilized;

    protected Transform playerTransform;

    Collider2D enemyCollider;
    protected Animator animator;
    public TextMeshPro damageText;

    bool antiMultipleDeathBool;

    protected virtual void Awake()
    {
        playerTransform = GameManager.Instance.GetPlayerBodyTransform();

        if (!TryGetComponent(out gemSpawner))
        {
            Debug.LogError($"{this} missing gemSpawner");
        }

        if (!TryGetComponent(out animator))
        {
            Debug.LogError($"{this} missing animator");
        }

        Collider2D eligibleCollider = GetComponent<BoxCollider2D>();
        if (eligibleCollider == null)
        {
            Debug.LogError($"{this} missing collider2D");
        }
        else
        {
            enemyCollider = eligibleCollider;
        }

        if (spriteRenderer == null)
        {
            Debug.LogError($"{this} missing SpriteRenderer");
        }

        actualMovementSpeed = movementSpeed;

        GameManager.Instance.OnGameWon += KillAtGameEnd;
    }

    private void Start()
    {
        StartCoroutine(Invincibility(spawnInvincibilityDuration));
    }

    protected virtual void Update()
    {
        if (isFreezed)
        {
            return;
        }

        if (Vector3.Distance(transform.position, playerTransform.position) <= attackRange)
        {
            Attack();
        }
        else
        {
            if (immobilized)
            {
                return;
            }
            float movementDistance = actualMovementSpeed * Time.deltaTime;
            Vector2 movementDirection = (Vector2) (playerTransform.position - transform.position).normalized;

            //bool obstacleAhead = Physics2D.BoxCast(transform.position, enemyCollider.bounds.extents, 0, movementDirection, movementDistance * 1.2f);
            //potrebbe prendere anche s� stesso.
            //possibili soluzioni:
            //enemyCollider.Cast()  --> bocciata perch� non funziona senza rigidbody da parte di uno dei due colliders
            //enemyCollider.Raycast() --> approvata ma non perfetta, non tiene conto delle dimensioni del collider che si deve spostare, servirebbero pi� raycast ma potrebbe impattare troppo la performance

            RaycastHit2D[] obstaclesAhead = new RaycastHit2D[1];
            int numberOfObstaclesAhead = enemyCollider.Raycast(movementDirection, obstaclesAhead, movementDistance + enemyCollider.bounds.extents.magnitude);

            //Mi muovo:
            if (numberOfObstaclesAhead > 0) //se ho davanti un ostacolo
            {
                BaseEnemy enemyAhead;
                if (obstaclesAhead[0].collider.gameObject.TryGetComponent(out enemyAhead)) //ed � un nemico
                {
                    if (enemyAhead.weight < weight) //pi� leggero di me
                    {
                        MoveTowardsPlayer(movementDistance);
                    }
                }
                else //o se non � un nemico (player)
                {
                    MoveTowardsPlayer(movementDistance);
                }
            }
            else // o se non ho ostacoli davanti
            {
                MoveTowardsPlayer(movementDistance);
            }
            
            if (movementDirection.x < 0)
            {
                spriteRenderer.flipX = true;
            }
            else if (movementDirection.x > 0)
            {
                spriteRenderer.flipX = false;
            }
        }
    }

    protected abstract void Attack();


    void MoveTowardsPlayer(float movementDistance)
    {
        transform.position = Vector3.MoveTowards(transform.position, playerTransform.position, movementDistance);
    }

    public void KillAtGameEnd()
    {
        if (GameManager.Instance.gameWon)
        {
            IsDead();
        }
    }
    public void IsDead()
    {
        if (antiMultipleDeathBool)
        {
            return;
        }
        antiMultipleDeathBool = true;

        StopAllCoroutines();
        if (!GameManager.Instance.gameWon)
        {
            if (GameManager.Instance.CheckIfOutOfBounds(transform.position))
            {
                GameManager.Instance.PickUpGem(dropQuantity);
            }
            else
            {
                gemSpawner.DropExpGems(dropQuantity);
            }
        }

        InstantiateDefeatedParticles();

        Destroy(gameObject);
    }

    private void InstantiateDefeatedParticles()
    {
        if (OnDeathParticlesToInstantiate.Count == 0)
        { return; }

        foreach (GameObject deathParticle in OnDeathParticlesToInstantiate)
        {
            Instantiate(deathParticle, transform.position, Quaternion.identity);
        }
    }

    public void SetMovementSpeed(float newMovementSpeed)
    {
        actualMovementSpeed = newMovementSpeed;
    }

    public IEnumerator HitVFX()
    {
        //obv alla buona
        spriteRenderer.color = new Color (spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, .5f);
        yield return new WaitForSeconds(.1f);
        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, 1);
    }

    public void StopFire()
    {
        isBurning = false;
        if (elementalStatusCoroutine != null)
        {
            StopCoroutine(elementalStatusCoroutine);
        }
        burningTimer = 0;
    }

    public void StopFreeze()
    {
        isFreezed = false;
        if (elementalStatusCoroutine != null)
        {
            StopCoroutine(elementalStatusCoroutine);
        }
        freezedTimer = 0;
        SetMovementSpeed(movementSpeed);
    }

    public virtual void SetElementalStatus(Element element)
    {
        switch (element)
        {
            case Element.Fire:
                isBurning = true;
                break;
            case Element.Ice:
                isFreezed = true;
                break;
            default:
                isBurning = false;
                isFreezed = false;
                break;
        }
        UpdateElementStatusVFX();
    }

    void UpdateElementStatusVFX()
    {
        if (isFreezed)
        {
            spriteRenderer.color = GameManager.Instance.elementsColors[(int)Element.Ice];
            animator.speed = 0;
        }
        else if (isBurning)
        {
            spriteRenderer.color = GameManager.Instance.elementsColors[(int)Element.Fire];
            animator.speed = 1;
        }
        else
        {
            spriteRenderer.color = Color.white;
            animator.speed = 1;
        }
    }

    //reminder: copia incollato da PlayerController con 1 modifica
    public IEnumerator Knockback(Vector3 pusherPosition, float knockbackDistance, float knockbackDuration)
    {
        if (immobilized)
        {
            yield break;
        }

        //funziona senza muri o ostacoli da non superare, altrimenti andrebbero checkati (o si usa il rb)
        immobilized = true;

        float realKnockbackDistance = knockbackDistance / weight;
        float realKnockbackDuration = knockbackDuration / weight;

        Vector3 startingPosition = transform.position;
        Vector3 destination = transform.position + (startingPosition - pusherPosition).normalized * realKnockbackDistance;

        float elapsedTime = 0;

        while (elapsedTime < realKnockbackDuration)
        {
            float normalizedElapsedTime = Mathf.InverseLerp(0.0f, realKnockbackDuration, elapsedTime);
            float evaluatedTimeByCurve = knockbackCurve.Evaluate(normalizedElapsedTime);

            transform.position = Vector2.Lerp(startingPosition, destination, evaluatedTimeByCurve);
            elapsedTime += Time.deltaTime;

            yield return null;
        }

        transform.position = destination;

        immobilized = false;
        yield return null;
    }

    IEnumerator Invincibility(float duration)
    {
        invincible = true;

        yield return new WaitForSeconds(duration);

        invincible = false;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnGameWon -= KillAtGameEnd;
    }
}